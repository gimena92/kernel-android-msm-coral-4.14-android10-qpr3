#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);
MODULE_INFO(name, KBUILD_MODNAME);

__visible struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

#ifdef CONFIG_RETPOLINE
MODULE_INFO(retpoline, "Y");
#endif

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x88f9a64e, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x8b2627a6, __VMLINUX_SYMBOL_STR(subsystem_restart_dev) },
	{ 0x373db350, __VMLINUX_SYMBOL_STR(kstrtoint) },
	{ 0x44b1d426, __VMLINUX_SYMBOL_STR(__dynamic_pr_debug) },
	{ 0x985558a1, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x2e0d2f7f, __VMLINUX_SYMBOL_STR(queue_work_on) },
	{ 0x2d3385d3, __VMLINUX_SYMBOL_STR(system_wq) },
	{ 0x20c55ae0, __VMLINUX_SYMBOL_STR(sscanf) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0xffce7c23, __VMLINUX_SYMBOL_STR(apr_set_modem_state) },
	{ 0x95d35054, __VMLINUX_SYMBOL_STR(apr_get_modem_state) },
	{ 0x41fac243, __VMLINUX_SYMBOL_STR(subsystem_get) },
	{ 0xa483fec, __VMLINUX_SYMBOL_STR(apr_get_q6_state) },
	{ 0xe2d5255a, __VMLINUX_SYMBOL_STR(strcmp) },
	{ 0x292c6a1a, __VMLINUX_SYMBOL_STR(__dynamic_dev_dbg) },
	{ 0xa896b642, __VMLINUX_SYMBOL_STR(of_property_read_string) },
	{ 0x28da7483, __VMLINUX_SYMBOL_STR(of_property_read_variable_u32_array) },
	{ 0x8f678b07, __VMLINUX_SYMBOL_STR(__stack_chk_guard) },
	{ 0xc5040ef2, __VMLINUX_SYMBOL_STR(sysfs_remove_group) },
	{ 0x3a0d61bc, __VMLINUX_SYMBOL_STR(subsystem_put) },
	{ 0xb570ef1e, __VMLINUX_SYMBOL_STR(kobject_del) },
	{ 0x6f4a684f, __VMLINUX_SYMBOL_STR(dev_err) },
	{ 0xd5845e7c, __VMLINUX_SYMBOL_STR(sysfs_create_group) },
	{ 0x268199cc, __VMLINUX_SYMBOL_STR(kobject_create_and_add) },
	{ 0x94df7e2e, __VMLINUX_SYMBOL_STR(kernel_kobj) },
	{ 0xc0738423, __VMLINUX_SYMBOL_STR(devm_kmalloc) },
	{ 0x3fc79f5c, __VMLINUX_SYMBOL_STR(platform_driver_unregister) },
	{ 0xefcc7c9, __VMLINUX_SYMBOL_STR(__platform_driver_register) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=apr_dlkm";

MODULE_ALIAS("of:N*T*Cqcom,adsp-loader");
MODULE_ALIAS("of:N*T*Cqcom,adsp-loaderC*");
