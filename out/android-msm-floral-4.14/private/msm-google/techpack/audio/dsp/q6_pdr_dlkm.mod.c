#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);
MODULE_INFO(name, KBUILD_MODNAME);

__visible struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

#ifdef CONFIG_RETPOLINE
MODULE_INFO(retpoline, "Y");
#endif

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x88f9a64e, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x4829a47e, __VMLINUX_SYMBOL_STR(memcpy) },
	{ 0x44b1d426, __VMLINUX_SYMBOL_STR(__dynamic_pr_debug) },
	{ 0x5c8b563a, __VMLINUX_SYMBOL_STR(srcu_notifier_call_chain) },
	{ 0x2efaac78, __VMLINUX_SYMBOL_STR(get_service_location) },
	{ 0xcba19920, __VMLINUX_SYMBOL_STR(srcu_init_notifier_head) },
	{ 0xd9fb38d8, __VMLINUX_SYMBOL_STR(service_notif_unregister_notifier) },
	{ 0xcba4bcae, __VMLINUX_SYMBOL_STR(service_notif_register_notifier) },
	{ 0xdc108d63, __VMLINUX_SYMBOL_STR(srcu_notifier_chain_unregister) },
	{ 0x985558a1, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x65614ce6, __VMLINUX_SYMBOL_STR(srcu_notifier_chain_register) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";

