#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);
MODULE_INFO(name, KBUILD_MODNAME);

__visible struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

#ifdef CONFIG_RETPOLINE
MODULE_INFO(retpoline, "Y");
#endif

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x88f9a64e, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0xee3c50ec, __VMLINUX_SYMBOL_STR(mhi_driver_unregister) },
	{ 0xa7fe7810, __VMLINUX_SYMBOL_STR(audio_notifier_register) },
	{ 0xe3f10ef3, __VMLINUX_SYMBOL_STR(mhi_driver_register) },
	{ 0xe44a0c78, __VMLINUX_SYMBOL_STR(dma_release_from_dev_coherent) },
	{ 0x226ba57, __VMLINUX_SYMBOL_STR(dma_alloc_from_dev_coherent) },
	{ 0x564246b2, __VMLINUX_SYMBOL_STR(of_get_property) },
	{ 0xcee0e5d3, __VMLINUX_SYMBOL_STR(of_parse_phandle) },
	{ 0xdbcb2800, __VMLINUX_SYMBOL_STR(mhi_device_put) },
	{ 0x44d25102, __VMLINUX_SYMBOL_STR(mhi_device_get_sync) },
	{ 0x32fce885, __VMLINUX_SYMBOL_STR(snd_ctl_boolean_mono_info) },
	{ 0xd7f99795, __VMLINUX_SYMBOL_STR(snd_soc_info_enum_double) },
	{ 0x59e7eb75, __VMLINUX_SYMBOL_STR(of_property_read_string_helper) },
	{ 0x11e1d939, __VMLINUX_SYMBOL_STR(__arch_copy_in_user) },
	{ 0xbffde8ec, __VMLINUX_SYMBOL_STR(compat_alloc_user_space) },
	{ 0xb77d33ac, __VMLINUX_SYMBOL_STR(request_firmware) },
	{ 0xe82fd0a6, __VMLINUX_SYMBOL_STR(release_firmware) },
	{ 0x6254e8e4, __VMLINUX_SYMBOL_STR(sysfs_create_groups) },
	{ 0xdc3fcbc9, __VMLINUX_SYMBOL_STR(__sw_hweight8) },
	{ 0xa1ecaedd, __VMLINUX_SYMBOL_STR(snd_soc_info_volsw) },
	{ 0xfeec4be4, __VMLINUX_SYMBOL_STR(snd_soc_add_platform_controls) },
	{ 0xda99fde5, __VMLINUX_SYMBOL_STR(cdev_del) },
	{ 0x41617c9d, __VMLINUX_SYMBOL_STR(device_destroy) },
	{ 0x6834ba2f, __VMLINUX_SYMBOL_STR(devm_iounmap) },
	{ 0x7485e15e, __VMLINUX_SYMBOL_STR(unregister_chrdev_region) },
	{ 0xc1bf3643, __VMLINUX_SYMBOL_STR(class_destroy) },
	{ 0x9f753acf, __VMLINUX_SYMBOL_STR(device_create) },
	{ 0xb66bcec0, __VMLINUX_SYMBOL_STR(cdev_add) },
	{ 0x7427997, __VMLINUX_SYMBOL_STR(cdev_init) },
	{ 0x29537c9e, __VMLINUX_SYMBOL_STR(alloc_chrdev_region) },
	{ 0xee52a932, __VMLINUX_SYMBOL_STR(__class_create) },
	{ 0xd8e484f0, __VMLINUX_SYMBOL_STR(register_chrdev_region) },
	{ 0x161afbb1, __VMLINUX_SYMBOL_STR(devm_ioremap_nocache) },
	{ 0x6b06fdce, __VMLINUX_SYMBOL_STR(delayed_work_timer_fn) },
	{ 0x5ee52022, __VMLINUX_SYMBOL_STR(init_timer_key) },
	{ 0x42d2feec, __VMLINUX_SYMBOL_STR(platform_get_resource_byname) },
	{ 0x75090002, __VMLINUX_SYMBOL_STR(queue_delayed_work_on) },
	{ 0x53ca0d03, __VMLINUX_SYMBOL_STR(arm_iommu_detach_device) },
	{ 0xb0888f81, __VMLINUX_SYMBOL_STR(arm_iommu_release_mapping) },
	{ 0x8b966278, __VMLINUX_SYMBOL_STR(arm_iommu_attach_device) },
	{ 0x64c464ce, __VMLINUX_SYMBOL_STR(arm_iommu_create_mapping) },
	{ 0x78bfce80, __VMLINUX_SYMBOL_STR(platform_bus_type) },
	{ 0x90479240, __VMLINUX_SYMBOL_STR(of_parse_phandle_with_args) },
	{ 0x53a0489d, __VMLINUX_SYMBOL_STR(of_property_read_u64) },
	{ 0x28da7483, __VMLINUX_SYMBOL_STR(of_property_read_variable_u32_array) },
	{ 0xa483fec, __VMLINUX_SYMBOL_STR(apr_get_q6_state) },
	{ 0x107e155a, __VMLINUX_SYMBOL_STR(of_find_property) },
	{ 0x3fec048f, __VMLINUX_SYMBOL_STR(sg_next) },
	{ 0x9c7ad484, __VMLINUX_SYMBOL_STR(remap_pfn_range) },
	{ 0xfadf2436, __VMLINUX_SYMBOL_STR(memstart_addr) },
	{ 0x53973de9, __VMLINUX_SYMBOL_STR(dma_buf_unmap_attachment) },
	{ 0x18e160b, __VMLINUX_SYMBOL_STR(dma_buf_end_cpu_access) },
	{ 0x1cbc40de, __VMLINUX_SYMBOL_STR(dma_buf_vunmap) },
	{ 0xfc307925, __VMLINUX_SYMBOL_STR(dma_buf_get) },
	{ 0x51e77c97, __VMLINUX_SYMBOL_STR(pfn_valid) },
	{ 0xd703abec, __VMLINUX_SYMBOL_STR(dummy_dma_ops) },
	{ 0xf5d299d4, __VMLINUX_SYMBOL_STR(dma_buf_vmap) },
	{ 0x267e72cf, __VMLINUX_SYMBOL_STR(dma_buf_begin_cpu_access) },
	{ 0x57aa3ef0, __VMLINUX_SYMBOL_STR(dma_buf_put) },
	{ 0xbf9bd9, __VMLINUX_SYMBOL_STR(dma_buf_detach) },
	{ 0x44bfb3f, __VMLINUX_SYMBOL_STR(dma_buf_map_attachment) },
	{ 0x5caf49c0, __VMLINUX_SYMBOL_STR(dma_buf_get_flags) },
	{ 0x8080f4a9, __VMLINUX_SYMBOL_STR(dma_buf_attach) },
	{ 0xeb45f8a5, __VMLINUX_SYMBOL_STR(ion_alloc) },
	{ 0x42305454, __VMLINUX_SYMBOL_STR(__pm_runtime_idle) },
	{ 0xc4fbd35d, __VMLINUX_SYMBOL_STR(__pm_runtime_resume) },
	{ 0x3a65d2a6, __VMLINUX_SYMBOL_STR(slim_driver_unregister) },
	{ 0xabe182da, __VMLINUX_SYMBOL_STR(slim_driver_register) },
	{ 0x7c97c8a4, __VMLINUX_SYMBOL_STR(__ll_sc_atomic_add_return) },
	{ 0x7a4497db, __VMLINUX_SYMBOL_STR(kzfree) },
	{ 0x64c6cac5, __VMLINUX_SYMBOL_STR(of_platform_depopulate) },
	{ 0xf1784e2a, __VMLINUX_SYMBOL_STR(of_platform_populate) },
	{ 0x3fc79f5c, __VMLINUX_SYMBOL_STR(platform_driver_unregister) },
	{ 0x2d6abf9a, __VMLINUX_SYMBOL_STR(kset_unregister) },
	{ 0xefcc7c9, __VMLINUX_SYMBOL_STR(__platform_driver_register) },
	{ 0xf9a482f9, __VMLINUX_SYMBOL_STR(msleep) },
	{ 0x2df50b0, __VMLINUX_SYMBOL_STR(jiffies) },
	{ 0xac7ecc44, __VMLINUX_SYMBOL_STR(kobject_uevent_env) },
	{ 0xc5477cb5, __VMLINUX_SYMBOL_STR(kobject_uevent) },
	{ 0xbd33b539, __VMLINUX_SYMBOL_STR(kobject_put) },
	{ 0xe2055b12, __VMLINUX_SYMBOL_STR(kobject_init_and_add) },
	{ 0x8b99f597, __VMLINUX_SYMBOL_STR(kset_create_and_add) },
	{ 0x94df7e2e, __VMLINUX_SYMBOL_STR(kernel_kobj) },
	{ 0x98cf60b3, __VMLINUX_SYMBOL_STR(strlen) },
	{ 0x1b17e06c, __VMLINUX_SYMBOL_STR(kstrtoll) },
	{ 0x619cb7dd, __VMLINUX_SYMBOL_STR(simple_read_from_buffer) },
	{ 0x28318305, __VMLINUX_SYMBOL_STR(snprintf) },
	{ 0x292c6a1a, __VMLINUX_SYMBOL_STR(__dynamic_dev_dbg) },
	{ 0x41d7b718, __VMLINUX_SYMBOL_STR(dev_warn) },
	{ 0x4f68e5c9, __VMLINUX_SYMBOL_STR(do_gettimeofday) },
	{ 0x6f4a684f, __VMLINUX_SYMBOL_STR(dev_err) },
	{ 0xdbd1083c, __VMLINUX_SYMBOL_STR(__ll_sc___cmpxchg_case_mb_4) },
	{ 0x97fdbab9, __VMLINUX_SYMBOL_STR(_raw_spin_unlock_irqrestore) },
	{ 0x96220280, __VMLINUX_SYMBOL_STR(_raw_spin_lock_irqsave) },
	{ 0x521d8898, __VMLINUX_SYMBOL_STR(apr_deregister) },
	{ 0xec2ac905, __VMLINUX_SYMBOL_STR(__ll_sc_atomic_sub_return) },
	{ 0x60ea2d6, __VMLINUX_SYMBOL_STR(kstrtoull) },
	{ 0x85df9b6c, __VMLINUX_SYMBOL_STR(strsep) },
	{ 0xe2d5255a, __VMLINUX_SYMBOL_STR(strcmp) },
	{ 0x3a30634b, __VMLINUX_SYMBOL_STR(__pm_relax) },
	{ 0x8617d991, __VMLINUX_SYMBOL_STR(pm_wakeup_ws_event) },
	{ 0x37befc70, __VMLINUX_SYMBOL_STR(jiffies_to_msecs) },
	{ 0x5792f848, __VMLINUX_SYMBOL_STR(strlcpy) },
	{ 0x79d5093f, __VMLINUX_SYMBOL_STR(wakeup_source_drop) },
	{ 0x61e0819b, __VMLINUX_SYMBOL_STR(wakeup_source_remove) },
	{ 0x9f9a21bf, __VMLINUX_SYMBOL_STR(debugfs_remove) },
	{ 0x2153ff95, __VMLINUX_SYMBOL_STR(debugfs_create_file) },
	{ 0x3ba5b837, __VMLINUX_SYMBOL_STR(wakeup_source_add) },
	{ 0xa08c5d68, __VMLINUX_SYMBOL_STR(wakeup_source_prepare) },
	{ 0x45452cf0, __VMLINUX_SYMBOL_STR(___ratelimit) },
	{ 0x12a38747, __VMLINUX_SYMBOL_STR(usleep_range) },
	{ 0x2e0d2f7f, __VMLINUX_SYMBOL_STR(queue_work_on) },
	{ 0x2d3385d3, __VMLINUX_SYMBOL_STR(system_wq) },
	{ 0x72409b16, __VMLINUX_SYMBOL_STR(apr_reset) },
	{ 0xc2b00af2, __VMLINUX_SYMBOL_STR(__init_waitqueue_head) },
	{ 0x7f02188f, __VMLINUX_SYMBOL_STR(__msecs_to_jiffies) },
	{ 0xf9a3efb9, __VMLINUX_SYMBOL_STR(__ll_sc_atomic_sub) },
	{ 0xa87cf413, __VMLINUX_SYMBOL_STR(clear_bit) },
	{ 0xae8c4d0c, __VMLINUX_SYMBOL_STR(set_bit) },
	{ 0x3c578bac, __VMLINUX_SYMBOL_STR(__wake_up) },
	{ 0x1f7386be, __VMLINUX_SYMBOL_STR(__ll_sc_atomic_add) },
	{ 0x21d3f39b, __VMLINUX_SYMBOL_STR(apr_register) },
	{ 0x9f317759, __VMLINUX_SYMBOL_STR(subsystem_restart) },
	{ 0x8ddd8aad, __VMLINUX_SYMBOL_STR(schedule_timeout) },
	{ 0x41acaf3c, __VMLINUX_SYMBOL_STR(finish_wait) },
	{ 0x7c580e7, __VMLINUX_SYMBOL_STR(prepare_to_wait_event) },
	{ 0xfe487975, __VMLINUX_SYMBOL_STR(init_wait_entry) },
	{ 0x8a2a5ff7, __VMLINUX_SYMBOL_STR(apr_send_pkt) },
	{ 0xb35dea8f, __VMLINUX_SYMBOL_STR(__arch_copy_to_user) },
	{ 0x88db9f48, __VMLINUX_SYMBOL_STR(__check_object_size) },
	{ 0xd2b09ce5, __VMLINUX_SYMBOL_STR(__kmalloc) },
	{ 0x84bc974b, __VMLINUX_SYMBOL_STR(__arch_copy_from_user) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0x8f678b07, __VMLINUX_SYMBOL_STR(__stack_chk_guard) },
	{ 0x58a6b0c5, __VMLINUX_SYMBOL_STR(misc_deregister) },
	{ 0xd9f3ffd5, __VMLINUX_SYMBOL_STR(misc_register) },
	{ 0xe2eb0ddf, __VMLINUX_SYMBOL_STR(__mutex_init) },
	{ 0xdcb764ad, __VMLINUX_SYMBOL_STR(memset) },
	{ 0x68f31cbd, __VMLINUX_SYMBOL_STR(__list_add_valid) },
	{ 0x4829a47e, __VMLINUX_SYMBOL_STR(memcpy) },
	{ 0x430c0213, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0xa5db1947, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0x4aacd53e, __VMLINUX_SYMBOL_STR(mutex_unlock) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0xe1537255, __VMLINUX_SYMBOL_STR(__list_del_entry_valid) },
	{ 0x44b1d426, __VMLINUX_SYMBOL_STR(__dynamic_pr_debug) },
	{ 0x5e38de65, __VMLINUX_SYMBOL_STR(mutex_lock) },
	{ 0x985558a1, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x89d8c863, __VMLINUX_SYMBOL_STR(blocking_notifier_call_chain) },
	{ 0x4cdb9af7, __VMLINUX_SYMBOL_STR(blocking_notifier_chain_unregister) },
	{ 0xb65bd286, __VMLINUX_SYMBOL_STR(blocking_notifier_chain_register) },
	{ 0x4576337f, __VMLINUX_SYMBOL_STR(atomic_notifier_call_chain) },
	{ 0x5606adb5, __VMLINUX_SYMBOL_STR(atomic_notifier_chain_unregister) },
	{ 0xc352cb02, __VMLINUX_SYMBOL_STR(atomic_notifier_chain_register) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=q6_notifier_dlkm,apr_dlkm";

MODULE_ALIAS("of:N*T*Cqcom,msm-audio-ion");
MODULE_ALIAS("of:N*T*Cqcom,msm-audio-ionC*");
MODULE_ALIAS("of:N*T*Ccirrus,msm-cirrus-playback");
MODULE_ALIAS("of:N*T*Ccirrus,msm-cirrus-playbackC*");
