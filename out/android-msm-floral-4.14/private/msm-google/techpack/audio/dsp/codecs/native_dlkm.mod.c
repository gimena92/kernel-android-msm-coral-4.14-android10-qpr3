#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);
MODULE_INFO(name, KBUILD_MODNAME);

__visible struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

#ifdef CONFIG_RETPOLINE
MODULE_INFO(retpoline, "Y");
#endif

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x88f9a64e, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0xa5c1c091, __VMLINUX_SYMBOL_STR(q6asm_enc_cfg_blk_qcelp) },
	{ 0x569cdac2, __VMLINUX_SYMBOL_STR(q6asm_enc_cfg_blk_g711) },
	{ 0xc0da3b40, __VMLINUX_SYMBOL_STR(q6asm_enc_cfg_blk_evrc) },
	{ 0x799334b, __VMLINUX_SYMBOL_STR(q6asm_media_format_block_wmapro) },
	{ 0xab618864, __VMLINUX_SYMBOL_STR(q6asm_media_format_block_wma) },
	{ 0x26f287cd, __VMLINUX_SYMBOL_STR(q6asm_set_encdec_chan_map) },
	{ 0x6008c9fd, __VMLINUX_SYMBOL_STR(q6asm_media_format_block_multi_aac) },
	{ 0x8241ccff, __VMLINUX_SYMBOL_STR(q6asm_enc_cfg_blk_pcm_native) },
	{ 0x45d0162e, __VMLINUX_SYMBOL_STR(q6asm_cfg_aac_sel_mix_coef) },
	{ 0x13041b66, __VMLINUX_SYMBOL_STR(q6asm_read_v2) },
	{ 0x5d020504, __VMLINUX_SYMBOL_STR(q6asm_set_softvolume_v2) },
	{ 0xc564f410, __VMLINUX_SYMBOL_STR(q6asm_media_format_block_pcm_format_support) },
	{ 0x172cc249, __VMLINUX_SYMBOL_STR(q6asm_audio_client_buf_alloc_contiguous) },
	{ 0xd4bd79ec, __VMLINUX_SYMBOL_STR(q6asm_open_read_write_v2) },
	{ 0xc5429b4f, __VMLINUX_SYMBOL_STR(msm_audio_effects_pbe_handler) },
	{ 0xa798531e, __VMLINUX_SYMBOL_STR(msm_audio_effects_volume_handler_v2) },
	{ 0xf20481c4, __VMLINUX_SYMBOL_STR(msm_audio_effects_popless_eq_handler) },
	{ 0xf33d2c2f, __VMLINUX_SYMBOL_STR(msm_audio_effects_bass_boost_handler) },
	{ 0xde157ae0, __VMLINUX_SYMBOL_STR(msm_audio_effects_reverb_handler) },
	{ 0x47a218f6, __VMLINUX_SYMBOL_STR(msm_audio_effects_virtualizer_handler) },
	{ 0xd5c1e5d, __VMLINUX_SYMBOL_STR(msm_audio_effects_is_effmodule_supp_in_top) },
	{ 0x51f259ea, __VMLINUX_SYMBOL_STR(q6asm_audio_client_buf_free_contiguous) },
	{ 0xfe812bd9, __VMLINUX_SYMBOL_STR(q6asm_media_format_block_g711) },
	{ 0xa41e939d, __VMLINUX_SYMBOL_STR(q6asm_media_format_block_ape) },
	{ 0x6fbd645d, __VMLINUX_SYMBOL_STR(q6asm_media_format_block_amrwbplus) },
	{ 0xdaef3dcb, __VMLINUX_SYMBOL_STR(q6asm_media_format_block_alac) },
	{ 0xc9104936, __VMLINUX_SYMBOL_STR(q6asm_enc_cfg_blk_pcm_v2) },
	{ 0xf6de57ac, __VMLINUX_SYMBOL_STR(q6asm_cfg_dual_mono_aac) },
	{ 0xcc282ce, __VMLINUX_SYMBOL_STR(q6asm_media_format_block_aac) },
	{ 0x72e207ff, __VMLINUX_SYMBOL_STR(q6asm_enable_sbrps) },
	{ 0xb7b7beb9, __VMLINUX_SYMBOL_STR(q6asm_enc_cfg_blk_pcm) },
	{ 0x2153ff95, __VMLINUX_SYMBOL_STR(debugfs_create_file) },
	{ 0x28318305, __VMLINUX_SYMBOL_STR(snprintf) },
	{ 0x370c0d6c, __VMLINUX_SYMBOL_STR(q6asm_open_write) },
	{ 0x7d36c8fc, __VMLINUX_SYMBOL_STR(device_init_wakeup) },
	{ 0x9e7f3408, __VMLINUX_SYMBOL_STR(q6asm_enc_cfg_blk_amrwb) },
	{ 0x2fd83cca, __VMLINUX_SYMBOL_STR(q6asm_enc_cfg_blk_amrnb) },
	{ 0x3c75c88c, __VMLINUX_SYMBOL_STR(q6asm_media_format_block_pcm) },
	{ 0x990c06ce, __VMLINUX_SYMBOL_STR(q6asm_enc_cfg_blk_aac) },
	{ 0xd09ab833, __VMLINUX_SYMBOL_STR(q6asm_reg_tx_overflow) },
	{ 0xec3d164c, __VMLINUX_SYMBOL_STR(q6asm_open_read) },
	{ 0x1f084d94, __VMLINUX_SYMBOL_STR(q6asm_open_read_write) },
	{ 0x3ce20d24, __VMLINUX_SYMBOL_STR(q6asm_audio_client_alloc) },
	{ 0x58a6b0c5, __VMLINUX_SYMBOL_STR(misc_deregister) },
	{ 0xd9f3ffd5, __VMLINUX_SYMBOL_STR(misc_register) },
	{ 0x686e4c25, __VMLINUX_SYMBOL_STR(q6asm_write) },
	{ 0x88db9f48, __VMLINUX_SYMBOL_STR(__check_object_size) },
	{ 0xf9a3efb9, __VMLINUX_SYMBOL_STR(__ll_sc_atomic_sub) },
	{ 0x334dac28, __VMLINUX_SYMBOL_STR(q6asm_is_cpu_buf_avail) },
	{ 0x7f33cf72, __VMLINUX_SYMBOL_STR(q6asm_read) },
	{ 0xb275fee4, __VMLINUX_SYMBOL_STR(q6asm_audio_client_buf_alloc) },
	{ 0xa75e0a64, __VMLINUX_SYMBOL_STR(q6asm_async_write) },
	{ 0xcf26e713, __VMLINUX_SYMBOL_STR(q6asm_async_read) },
	{ 0x8ddd8aad, __VMLINUX_SYMBOL_STR(schedule_timeout) },
	{ 0x7f02188f, __VMLINUX_SYMBOL_STR(__msecs_to_jiffies) },
	{ 0x47139863, __VMLINUX_SYMBOL_STR(q6asm_memory_map) },
	{ 0xbbe22f8, __VMLINUX_SYMBOL_STR(msm_audio_ion_import) },
	{ 0x2df376a3, __VMLINUX_SYMBOL_STR(pm_stay_awake) },
	{ 0xd364cd4c, __VMLINUX_SYMBOL_STR(mutex_trylock) },
	{ 0xb195cb69, __VMLINUX_SYMBOL_STR(q6asm_get_session_time) },
	{ 0x84bc974b, __VMLINUX_SYMBOL_STR(__arch_copy_from_user) },
	{ 0xb35dea8f, __VMLINUX_SYMBOL_STR(__arch_copy_to_user) },
	{ 0xc2b00af2, __VMLINUX_SYMBOL_STR(__init_waitqueue_head) },
	{ 0xe2eb0ddf, __VMLINUX_SYMBOL_STR(__mutex_init) },
	{ 0x3bae2f9e, __VMLINUX_SYMBOL_STR(q6asm_set_io_mode) },
	{ 0x41acaf3c, __VMLINUX_SYMBOL_STR(finish_wait) },
	{ 0x1000e51, __VMLINUX_SYMBOL_STR(schedule) },
	{ 0x7c580e7, __VMLINUX_SYMBOL_STR(prepare_to_wait_event) },
	{ 0xfe487975, __VMLINUX_SYMBOL_STR(init_wait_entry) },
	{ 0x9f9a21bf, __VMLINUX_SYMBOL_STR(debugfs_remove) },
	{ 0xe2d5b3dc, __VMLINUX_SYMBOL_STR(q6asm_audio_client_free) },
	{ 0xb7b409fd, __VMLINUX_SYMBOL_STR(q6asm_memory_unmap) },
	{ 0x425c362e, __VMLINUX_SYMBOL_STR(pm_relax) },
	{ 0xb2f4a5e0, __VMLINUX_SYMBOL_STR(msm_audio_ion_free) },
	{ 0x45452cf0, __VMLINUX_SYMBOL_STR(___ratelimit) },
	{ 0x4371e9fe, __VMLINUX_SYMBOL_STR(q6asm_cmd) },
	{ 0x7e2f7c16, __VMLINUX_SYMBOL_STR(q6asm_run) },
	{ 0x68f31cbd, __VMLINUX_SYMBOL_STR(__list_add_valid) },
	{ 0x430c0213, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0xa5db1947, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0x619cb7dd, __VMLINUX_SYMBOL_STR(simple_read_from_buffer) },
	{ 0x4aacd53e, __VMLINUX_SYMBOL_STR(mutex_unlock) },
	{ 0x4ca9669f, __VMLINUX_SYMBOL_STR(scnprintf) },
	{ 0x5e38de65, __VMLINUX_SYMBOL_STR(mutex_lock) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x4829a47e, __VMLINUX_SYMBOL_STR(memcpy) },
	{ 0xe1537255, __VMLINUX_SYMBOL_STR(__list_del_entry_valid) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0xdcb764ad, __VMLINUX_SYMBOL_STR(memset) },
	{ 0x8f678b07, __VMLINUX_SYMBOL_STR(__stack_chk_guard) },
	{ 0x8c5ad368, __VMLINUX_SYMBOL_STR(q6asm_get_buf_index_from_token) },
	{ 0x44b1d426, __VMLINUX_SYMBOL_STR(__dynamic_pr_debug) },
	{ 0x97fdbab9, __VMLINUX_SYMBOL_STR(_raw_spin_unlock_irqrestore) },
	{ 0x3c578bac, __VMLINUX_SYMBOL_STR(__wake_up) },
	{ 0x985558a1, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x1f7386be, __VMLINUX_SYMBOL_STR(__ll_sc_atomic_add) },
	{ 0x96220280, __VMLINUX_SYMBOL_STR(_raw_spin_lock_irqsave) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=q6_dlkm,platform_dlkm";

