#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);
MODULE_INFO(name, KBUILD_MODNAME);

__visible struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

#ifdef CONFIG_RETPOLINE
MODULE_INFO(retpoline, "Y");
#endif

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x88f9a64e, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0xb4b47e3b, __VMLINUX_SYMBOL_STR(msm_audio_ion_mmap) },
	{ 0x41d22a52, __VMLINUX_SYMBOL_STR(msm_audio_populate_upper_32_bits) },
	{ 0x9681bbcf, __VMLINUX_SYMBOL_STR(msm_audio_ion_alloc) },
	{ 0x97fdbab9, __VMLINUX_SYMBOL_STR(_raw_spin_unlock_irqrestore) },
	{ 0x96220280, __VMLINUX_SYMBOL_STR(_raw_spin_lock_irqsave) },
	{ 0x72409b16, __VMLINUX_SYMBOL_STR(apr_reset) },
	{ 0x1f7386be, __VMLINUX_SYMBOL_STR(__ll_sc_atomic_add) },
	{ 0x21d3f39b, __VMLINUX_SYMBOL_STR(apr_register) },
	{ 0xf9a3efb9, __VMLINUX_SYMBOL_STR(__ll_sc_atomic_sub) },
	{ 0x521d8898, __VMLINUX_SYMBOL_STR(apr_deregister) },
	{ 0xb2f4a5e0, __VMLINUX_SYMBOL_STR(msm_audio_ion_free) },
	{ 0x8a2a5ff7, __VMLINUX_SYMBOL_STR(apr_send_pkt) },
	{ 0x8f3dbdb2, __VMLINUX_SYMBOL_STR(input_close_device) },
	{ 0xbd0b6d40, __VMLINUX_SYMBOL_STR(input_unregister_handle) },
	{ 0x95ef514e, __VMLINUX_SYMBOL_STR(input_open_device) },
	{ 0x8cfc7a6f, __VMLINUX_SYMBOL_STR(input_register_handle) },
	{ 0x9f46dead, __VMLINUX_SYMBOL_STR(input_unregister_handler) },
	{ 0xe748485b, __VMLINUX_SYMBOL_STR(input_register_handler) },
	{ 0x26411dc5, __VMLINUX_SYMBOL_STR(input_unregister_device) },
	{ 0x8ddd8aad, __VMLINUX_SYMBOL_STR(schedule_timeout) },
	{ 0x2df50b0, __VMLINUX_SYMBOL_STR(jiffies) },
	{ 0x41acaf3c, __VMLINUX_SYMBOL_STR(finish_wait) },
	{ 0x1000e51, __VMLINUX_SYMBOL_STR(schedule) },
	{ 0x7c580e7, __VMLINUX_SYMBOL_STR(prepare_to_wait_event) },
	{ 0xfe487975, __VMLINUX_SYMBOL_STR(init_wait_entry) },
	{ 0xe2d5255a, __VMLINUX_SYMBOL_STR(strcmp) },
	{ 0x46e6ddc5, __VMLINUX_SYMBOL_STR(input_event) },
	{ 0x23936b78, __VMLINUX_SYMBOL_STR(input_set_abs_params) },
	{ 0x727c08c4, __VMLINUX_SYMBOL_STR(input_allocate_device) },
	{ 0x88db9f48, __VMLINUX_SYMBOL_STR(__check_object_size) },
	{ 0xd2b09ce5, __VMLINUX_SYMBOL_STR(__kmalloc) },
	{ 0x4829a47e, __VMLINUX_SYMBOL_STR(memcpy) },
	{ 0x8617d991, __VMLINUX_SYMBOL_STR(pm_wakeup_ws_event) },
	{ 0xcddde97d, __VMLINUX_SYMBOL_STR(input_free_device) },
	{ 0x33d941c4, __VMLINUX_SYMBOL_STR(input_register_device) },
	{ 0x24428be5, __VMLINUX_SYMBOL_STR(strncpy_from_user) },
	{ 0xc2b00af2, __VMLINUX_SYMBOL_STR(__init_waitqueue_head) },
	{ 0x79d5093f, __VMLINUX_SYMBOL_STR(wakeup_source_drop) },
	{ 0x61e0819b, __VMLINUX_SYMBOL_STR(wakeup_source_remove) },
	{ 0x3c578bac, __VMLINUX_SYMBOL_STR(__wake_up) },
	{ 0xe2eb0ddf, __VMLINUX_SYMBOL_STR(__mutex_init) },
	{ 0x3ba5b837, __VMLINUX_SYMBOL_STR(wakeup_source_add) },
	{ 0xa08c5d68, __VMLINUX_SYMBOL_STR(wakeup_source_prepare) },
	{ 0x430c0213, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0xa5db1947, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0xdbd1083c, __VMLINUX_SYMBOL_STR(__ll_sc___cmpxchg_case_mb_4) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0xb35dea8f, __VMLINUX_SYMBOL_STR(__arch_copy_to_user) },
	{ 0xdcb764ad, __VMLINUX_SYMBOL_STR(memset) },
	{ 0x84bc974b, __VMLINUX_SYMBOL_STR(__arch_copy_from_user) },
	{ 0x8f678b07, __VMLINUX_SYMBOL_STR(__stack_chk_guard) },
	{ 0x4aacd53e, __VMLINUX_SYMBOL_STR(mutex_unlock) },
	{ 0x5e38de65, __VMLINUX_SYMBOL_STR(mutex_lock) },
	{ 0x58a6b0c5, __VMLINUX_SYMBOL_STR(misc_deregister) },
	{ 0x44b1d426, __VMLINUX_SYMBOL_STR(__dynamic_pr_debug) },
	{ 0x985558a1, __VMLINUX_SYMBOL_STR(printk) },
	{ 0xd9f3ffd5, __VMLINUX_SYMBOL_STR(misc_register) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=q6_dlkm,apr_dlkm";

MODULE_ALIAS("input:b*v*p*e*-e*1,*3,*k*14A,*r*a*0,*1,*m*l*s*f*w*");
MODULE_ALIAS("input:b*v*p*e*-e*1,*3,*k*14A,*r*a*35,*36,*m*l*s*f*w*");
