#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);
MODULE_INFO(name, KBUILD_MODNAME);

__visible struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

#ifdef CONFIG_RETPOLINE
MODULE_INFO(retpoline, "Y");
#endif

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x88f9a64e, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0xb37cb2b2, __VMLINUX_SYMBOL_STR(subsys_notif_unregister_notifier) },
	{ 0xab8be69c, __VMLINUX_SYMBOL_STR(subsys_notif_register_notifier) },
	{ 0x5c8b563a, __VMLINUX_SYMBOL_STR(srcu_notifier_call_chain) },
	{ 0xcb0149a3, __VMLINUX_SYMBOL_STR(audio_pdr_deregister) },
	{ 0xf66a8089, __VMLINUX_SYMBOL_STR(audio_pdr_register) },
	{ 0xcba19920, __VMLINUX_SYMBOL_STR(srcu_init_notifier_head) },
	{ 0xe2eb0ddf, __VMLINUX_SYMBOL_STR(__mutex_init) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0x458155b2, __VMLINUX_SYMBOL_STR(audio_pdr_service_register) },
	{ 0x65614ce6, __VMLINUX_SYMBOL_STR(srcu_notifier_chain_register) },
	{ 0x8f678b07, __VMLINUX_SYMBOL_STR(__stack_chk_guard) },
	{ 0x68f31cbd, __VMLINUX_SYMBOL_STR(__list_add_valid) },
	{ 0x5792f848, __VMLINUX_SYMBOL_STR(strlcpy) },
	{ 0x430c0213, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0xa5db1947, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0x4aacd53e, __VMLINUX_SYMBOL_STR(mutex_unlock) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0xe1537255, __VMLINUX_SYMBOL_STR(__list_del_entry_valid) },
	{ 0xdc108d63, __VMLINUX_SYMBOL_STR(srcu_notifier_chain_unregister) },
	{ 0x10fed888, __VMLINUX_SYMBOL_STR(audio_pdr_service_deregister) },
	{ 0xe2d5255a, __VMLINUX_SYMBOL_STR(strcmp) },
	{ 0x44b1d426, __VMLINUX_SYMBOL_STR(__dynamic_pr_debug) },
	{ 0x985558a1, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x5e38de65, __VMLINUX_SYMBOL_STR(mutex_lock) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=q6_pdr_dlkm";

