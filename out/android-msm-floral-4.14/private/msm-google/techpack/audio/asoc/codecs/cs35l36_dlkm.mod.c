#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);
MODULE_INFO(name, KBUILD_MODNAME);

__visible struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

#ifdef CONFIG_RETPOLINE
MODULE_INFO(retpoline, "Y");
#endif

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x88f9a64e, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0xbf7a98fc, __VMLINUX_SYMBOL_STR(snd_soc_dapm_put_volsw) },
	{ 0xeb3e1ef6, __VMLINUX_SYMBOL_STR(snd_soc_dapm_get_volsw) },
	{ 0x1e370286, __VMLINUX_SYMBOL_STR(snd_soc_dapm_put_enum_double) },
	{ 0xfec92dd4, __VMLINUX_SYMBOL_STR(snd_soc_dapm_get_enum_double) },
	{ 0xa14673f3, __VMLINUX_SYMBOL_STR(snd_soc_put_enum_double) },
	{ 0x14e21999, __VMLINUX_SYMBOL_STR(snd_soc_get_enum_double) },
	{ 0xd7f99795, __VMLINUX_SYMBOL_STR(snd_soc_info_enum_double) },
	{ 0x855d07c6, __VMLINUX_SYMBOL_STR(snd_soc_put_volsw) },
	{ 0xb6a3c1a2, __VMLINUX_SYMBOL_STR(snd_soc_get_volsw) },
	{ 0xa1ecaedd, __VMLINUX_SYMBOL_STR(snd_soc_info_volsw) },
	{ 0x41797e73, __VMLINUX_SYMBOL_STR(snd_soc_put_volsw_sx) },
	{ 0xc902b26f, __VMLINUX_SYMBOL_STR(snd_soc_get_volsw_sx) },
	{ 0xca63048e, __VMLINUX_SYMBOL_STR(snd_soc_info_volsw_sx) },
	{ 0xe56a9336, __VMLINUX_SYMBOL_STR(snd_pcm_format_width) },
	{ 0xc2be1b3, __VMLINUX_SYMBOL_STR(snd_pcm_hw_constraint_list) },
	{ 0x51da131a, __VMLINUX_SYMBOL_STR(msm_crus_check_set_setting) },
	{ 0x44b1d426, __VMLINUX_SYMBOL_STR(__dynamic_pr_debug) },
	{ 0x291ec71b, __VMLINUX_SYMBOL_STR(msm_crus_store_imped) },
	{ 0x292c6a1a, __VMLINUX_SYMBOL_STR(__dynamic_dev_dbg) },
	{ 0x44196fa4, __VMLINUX_SYMBOL_STR(snd_soc_dapm_sync) },
	{ 0x855f6fef, __VMLINUX_SYMBOL_STR(snd_soc_dapm_ignore_suspend) },
	{ 0xe2d5255a, __VMLINUX_SYMBOL_STR(strcmp) },
	{ 0x1226ef78, __VMLINUX_SYMBOL_STR(dev_crit) },
	{ 0x3b569d72, __VMLINUX_SYMBOL_STR(regmap_bulk_read) },
	{ 0xe83597a9, __VMLINUX_SYMBOL_STR(snd_soc_unregister_codec) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0x391bb3bb, __VMLINUX_SYMBOL_STR(snd_soc_register_codec) },
	{ 0xee2cf1b, __VMLINUX_SYMBOL_STR(devm_request_threaded_irq) },
	{ 0x50e85e4d, __VMLINUX_SYMBOL_STR(regmap_update_bits_base) },
	{ 0x233e820f, __VMLINUX_SYMBOL_STR(regmap_write) },
	{ 0xbfdf95f4, __VMLINUX_SYMBOL_STR(of_get_child_by_name) },
	{ 0x107e155a, __VMLINUX_SYMBOL_STR(of_find_property) },
	{ 0x28da7483, __VMLINUX_SYMBOL_STR(of_property_read_variable_u32_array) },
	{ 0x985558a1, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x85f5d75, __VMLINUX_SYMBOL_STR(regmap_register_patch) },
	{ 0x433c6216, __VMLINUX_SYMBOL_STR(regmap_read) },
	{ 0x12a38747, __VMLINUX_SYMBOL_STR(usleep_range) },
	{ 0xdefb348d, __VMLINUX_SYMBOL_STR(_dev_info) },
	{ 0x9f495b7, __VMLINUX_SYMBOL_STR(devm_gpiod_get_optional) },
	{ 0x6982b0ef, __VMLINUX_SYMBOL_STR(dev_set_name) },
	{ 0xa896b642, __VMLINUX_SYMBOL_STR(of_property_read_string) },
	{ 0xe9f56873, __VMLINUX_SYMBOL_STR(regulator_bulk_enable) },
	{ 0x963232e6, __VMLINUX_SYMBOL_STR(devm_regulator_bulk_get) },
	{ 0x28ca4d2a, __VMLINUX_SYMBOL_STR(gpiod_set_value_cansleep) },
	{ 0x64dc17fd, __VMLINUX_SYMBOL_STR(regulator_bulk_disable) },
	{ 0x6f4a684f, __VMLINUX_SYMBOL_STR(dev_err) },
	{ 0x52186e89, __VMLINUX_SYMBOL_STR(__devm_regmap_init_i2c) },
	{ 0xc0738423, __VMLINUX_SYMBOL_STR(devm_kmalloc) },
	{ 0x8f678b07, __VMLINUX_SYMBOL_STR(__stack_chk_guard) },
	{ 0x1116156, __VMLINUX_SYMBOL_STR(i2c_del_driver) },
	{ 0xeea16081, __VMLINUX_SYMBOL_STR(i2c_register_driver) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=q6_dlkm";

MODULE_ALIAS("of:N*T*Ccirrus,cs35l36");
MODULE_ALIAS("of:N*T*Ccirrus,cs35l36C*");
MODULE_ALIAS("i2c:cs35l36");
