#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);
MODULE_INFO(name, KBUILD_MODNAME);

__visible struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

#ifdef CONFIG_RETPOLINE
MODULE_INFO(retpoline, "Y");
#endif

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x88f9a64e, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x29da51bb, __VMLINUX_SYMBOL_STR(afe_unvote_lpass_core_hw) },
	{ 0x1fb586f7, __VMLINUX_SYMBOL_STR(q6core_is_avs_up) },
	{ 0x672a537e, __VMLINUX_SYMBOL_STR(afe_vote_lpass_core_hw) },
	{ 0x1c47e88b, __VMLINUX_SYMBOL_STR(clk_hw_get_num_parents) },
	{ 0x45452cf0, __VMLINUX_SYMBOL_STR(___ratelimit) },
	{ 0x9f788997, __VMLINUX_SYMBOL_STR(afe_set_lpass_clk_cfg) },
	{ 0xa018a4d1, __VMLINUX_SYMBOL_STR(of_clk_add_provider) },
	{ 0x91b30caa, __VMLINUX_SYMBOL_STR(of_clk_src_onecell_get) },
	{ 0xe1138f11, __VMLINUX_SYMBOL_STR(devm_clk_register) },
	{ 0xf24b3dfe, __VMLINUX_SYMBOL_STR(__ioremap) },
	{ 0x5f986d19, __VMLINUX_SYMBOL_STR(pinctrl_select_state) },
	{ 0xa2033a72, __VMLINUX_SYMBOL_STR(devm_pinctrl_put) },
	{ 0x19e083d7, __VMLINUX_SYMBOL_STR(pinctrl_lookup_state) },
	{ 0x4e4540de, __VMLINUX_SYMBOL_STR(devm_pinctrl_get) },
	{ 0x28da7483, __VMLINUX_SYMBOL_STR(of_property_read_variable_u32_array) },
	{ 0x4829a47e, __VMLINUX_SYMBOL_STR(memcpy) },
	{ 0x3336736f, __VMLINUX_SYMBOL_STR(complete) },
	{ 0x69172e7f, __VMLINUX_SYMBOL_STR(destroy_ramdump_device) },
	{ 0x7ef33997, __VMLINUX_SYMBOL_STR(debugfs_remove_recursive) },
	{ 0x88bfa7e, __VMLINUX_SYMBOL_STR(cancel_work_sync) },
	{ 0x2e0d2f7f, __VMLINUX_SYMBOL_STR(queue_work_on) },
	{ 0x2d3385d3, __VMLINUX_SYMBOL_STR(system_wq) },
	{ 0xb162aa1c, __VMLINUX_SYMBOL_STR(debugfs_create_bool) },
	{ 0x5741ee97, __VMLINUX_SYMBOL_STR(debugfs_create_dir) },
	{ 0x980198bc, __VMLINUX_SYMBOL_STR(component_unbind_all) },
	{ 0x7ede67b, __VMLINUX_SYMBOL_STR(component_bind_all) },
	{ 0xdefb348d, __VMLINUX_SYMBOL_STR(_dev_info) },
	{ 0x6a42d5e1, __VMLINUX_SYMBOL_STR(create_ramdump_device) },
	{ 0xe44a0c78, __VMLINUX_SYMBOL_STR(dma_release_from_dev_coherent) },
	{ 0x6af6e993, __VMLINUX_SYMBOL_STR(do_ramdump) },
	{ 0x226ba57, __VMLINUX_SYMBOL_STR(dma_alloc_from_dev_coherent) },
	{ 0xd703abec, __VMLINUX_SYMBOL_STR(dummy_dma_ops) },
	{ 0xe2d5255a, __VMLINUX_SYMBOL_STR(strcmp) },
	{ 0xbe69f92a, __VMLINUX_SYMBOL_STR(wait_for_completion_timeout) },
	{ 0x4c37dfd7, __VMLINUX_SYMBOL_STR(component_master_del) },
	{ 0x2c4ec8de, __VMLINUX_SYMBOL_STR(component_master_add_with_match) },
	{ 0xc3012448, __VMLINUX_SYMBOL_STR(arch_setup_dma_ops) },
	{ 0xc2b00af2, __VMLINUX_SYMBOL_STR(__init_waitqueue_head) },
	{ 0xff9b9514, __VMLINUX_SYMBOL_STR(component_match_add_release) },
	{ 0xc9288e23, __VMLINUX_SYMBOL_STR(devm_kfree) },
	{ 0x9319041a, __VMLINUX_SYMBOL_STR(of_parse_phandle_with_fixed_args) },
	{ 0xb5f8070c, __VMLINUX_SYMBOL_STR(of_count_phandle_with_args) },
	{ 0xa896b642, __VMLINUX_SYMBOL_STR(of_property_read_string) },
	{ 0xc0738423, __VMLINUX_SYMBOL_STR(devm_kmalloc) },
	{ 0x3fc79f5c, __VMLINUX_SYMBOL_STR(platform_driver_unregister) },
	{ 0xefcc7c9, __VMLINUX_SYMBOL_STR(__platform_driver_register) },
	{ 0x68f31cbd, __VMLINUX_SYMBOL_STR(__list_add_valid) },
	{ 0x449ad0a7, __VMLINUX_SYMBOL_STR(memcmp) },
	{ 0xb77d33ac, __VMLINUX_SYMBOL_STR(request_firmware) },
	{ 0xe1537255, __VMLINUX_SYMBOL_STR(__list_del_entry_valid) },
	{ 0xe82fd0a6, __VMLINUX_SYMBOL_STR(release_firmware) },
	{ 0x88db9f48, __VMLINUX_SYMBOL_STR(__check_object_size) },
	{ 0xdcb764ad, __VMLINUX_SYMBOL_STR(memset) },
	{ 0x84bc974b, __VMLINUX_SYMBOL_STR(__arch_copy_from_user) },
	{ 0xd2b09ce5, __VMLINUX_SYMBOL_STR(__kmalloc) },
	{ 0xae8c4d0c, __VMLINUX_SYMBOL_STR(set_bit) },
	{ 0xc0a3d105, __VMLINUX_SYMBOL_STR(find_next_bit) },
	{ 0x455f237e, __VMLINUX_SYMBOL_STR(snd_hwdep_new) },
	{ 0x98cf60b3, __VMLINUX_SYMBOL_STR(strlen) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0xe2eb0ddf, __VMLINUX_SYMBOL_STR(__mutex_init) },
	{ 0x430c0213, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0xa5db1947, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0x433c6216, __VMLINUX_SYMBOL_STR(regmap_read) },
	{ 0x50e85e4d, __VMLINUX_SYMBOL_STR(regmap_update_bits_base) },
	{ 0x4aacd53e, __VMLINUX_SYMBOL_STR(mutex_unlock) },
	{ 0x5e38de65, __VMLINUX_SYMBOL_STR(mutex_lock) },
	{ 0x985558a1, __VMLINUX_SYMBOL_STR(printk) },
	{ 0xa49b6667, __VMLINUX_SYMBOL_STR(wcd9xxx_slim_bulk_write) },
	{ 0xaa4e9a2, __VMLINUX_SYMBOL_STR(snd_soc_write) },
	{ 0x3b11a732, __VMLINUX_SYMBOL_STR(snd_soc_read) },
	{ 0x44b1d426, __VMLINUX_SYMBOL_STR(__dynamic_pr_debug) },
	{ 0x12a38747, __VMLINUX_SYMBOL_STR(usleep_range) },
	{ 0x5c2f08e5, __VMLINUX_SYMBOL_STR(snd_soc_update_bits) },
	{ 0xf9c0b663, __VMLINUX_SYMBOL_STR(strlcat) },
	{ 0x28318305, __VMLINUX_SYMBOL_STR(snprintf) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0x292c6a1a, __VMLINUX_SYMBOL_STR(__dynamic_dev_dbg) },
	{ 0x6f4a684f, __VMLINUX_SYMBOL_STR(dev_err) },
	{ 0x8f678b07, __VMLINUX_SYMBOL_STR(__stack_chk_guard) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=q6_dlkm,wcd_core_dlkm";

MODULE_ALIAS("of:N*T*Cqcom,wcd-dsp-mgr");
MODULE_ALIAS("of:N*T*Cqcom,wcd-dsp-mgrC*");
MODULE_ALIAS("of:N*T*Cqcom,audio-ref-clk");
MODULE_ALIAS("of:N*T*Cqcom,audio-ref-clkC*");
