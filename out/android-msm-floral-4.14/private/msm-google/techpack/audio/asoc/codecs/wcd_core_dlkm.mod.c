#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);
MODULE_INFO(name, KBUILD_MODNAME);

__visible struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

#ifdef CONFIG_RETPOLINE
MODULE_INFO(retpoline, "Y");
#endif

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x88f9a64e, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x564246b2, __VMLINUX_SYMBOL_STR(of_get_property) },
	{ 0x59e7eb75, __VMLINUX_SYMBOL_STR(of_property_read_string_helper) },
	{ 0x963232e6, __VMLINUX_SYMBOL_STR(devm_regulator_bulk_get) },
	{ 0xe26c1864, __VMLINUX_SYMBOL_STR(regulator_set_load) },
	{ 0x67beed4, __VMLINUX_SYMBOL_STR(regulator_set_voltage) },
	{ 0x369e3f0, __VMLINUX_SYMBOL_STR(regulator_count_voltages) },
	{ 0x1154081c, __VMLINUX_SYMBOL_STR(regulator_enable) },
	{ 0x6cde8e6d, __VMLINUX_SYMBOL_STR(regulator_disable) },
	{ 0xfe990052, __VMLINUX_SYMBOL_STR(gpio_free) },
	{ 0xa2033a72, __VMLINUX_SYMBOL_STR(devm_pinctrl_put) },
	{ 0x47229b5c, __VMLINUX_SYMBOL_STR(gpio_request) },
	{ 0x19e083d7, __VMLINUX_SYMBOL_STR(pinctrl_lookup_state) },
	{ 0x4e4540de, __VMLINUX_SYMBOL_STR(devm_pinctrl_get) },
	{ 0x5f986d19, __VMLINUX_SYMBOL_STR(pinctrl_select_state) },
	{ 0xcc842dc5, __VMLINUX_SYMBOL_STR(gpiod_get_raw_value_cansleep) },
	{ 0x2f3162be, __VMLINUX_SYMBOL_STR(of_find_device_by_node) },
	{ 0x23bc59d4, __VMLINUX_SYMBOL_STR(regmap_multi_reg_write) },
	{ 0xac68ce7b, __VMLINUX_SYMBOL_STR(regcache_cache_only) },
	{ 0xcfc0eb9a, __VMLINUX_SYMBOL_STR(pm_qos_remove_request) },
	{ 0xb2532ea5, __VMLINUX_SYMBOL_STR(pm_qos_add_request) },
	{ 0xc2b00af2, __VMLINUX_SYMBOL_STR(__init_waitqueue_head) },
	{ 0x1cf816a0, __VMLINUX_SYMBOL_STR(__devm_regmap_init) },
	{ 0x48cb3fa6, __VMLINUX_SYMBOL_STR(of_device_is_compatible) },
	{ 0xcee0e5d3, __VMLINUX_SYMBOL_STR(of_parse_phandle) },
	{ 0x28da7483, __VMLINUX_SYMBOL_STR(of_property_read_variable_u32_array) },
	{ 0xc0a3d105, __VMLINUX_SYMBOL_STR(find_next_bit) },
	{ 0x871fb55e, __VMLINUX_SYMBOL_STR(slim_disconnect_ports) },
	{ 0x23a4433f, __VMLINUX_SYMBOL_STR(slim_connect_src) },
	{ 0xd0e7735f, __VMLINUX_SYMBOL_STR(slim_control_ch) },
	{ 0xe59c9fa, __VMLINUX_SYMBOL_STR(slim_connect_sink) },
	{ 0x97847e42, __VMLINUX_SYMBOL_STR(slim_define_ch) },
	{ 0x3fd048d6, __VMLINUX_SYMBOL_STR(slim_dealloc_ch) },
	{ 0xb11f27bf, __VMLINUX_SYMBOL_STR(slim_query_ch) },
	{ 0x123ee5b0, __VMLINUX_SYMBOL_STR(slim_get_slaveport) },
	{ 0x8e3d2774, __VMLINUX_SYMBOL_STR(irq_domain_remove) },
	{ 0x5eed7ae1, __VMLINUX_SYMBOL_STR(__irq_domain_add) },
	{ 0x301250c3, __VMLINUX_SYMBOL_STR(irq_domain_simple_ops) },
	{ 0x430c0213, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0xa5db1947, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0x9b9571a3, __VMLINUX_SYMBOL_STR(platform_get_irq_byname) },
	{ 0x87db8856, __VMLINUX_SYMBOL_STR(gpiod_to_irq) },
	{ 0xb885925a, __VMLINUX_SYMBOL_STR(of_get_named_gpio_flags) },
	{ 0x8a2d0e1f, __VMLINUX_SYMBOL_STR(irq_get_irq_data) },
	{ 0x3fc79f5c, __VMLINUX_SYMBOL_STR(platform_driver_unregister) },
	{ 0xefcc7c9, __VMLINUX_SYMBOL_STR(__platform_driver_register) },
	{ 0xeea93233, __VMLINUX_SYMBOL_STR(regmap_bulk_write) },
	{ 0xfe916dc6, __VMLINUX_SYMBOL_STR(hex_dump_to_buffer) },
	{ 0x14d57932, __VMLINUX_SYMBOL_STR(handle_nested_irq) },
	{ 0x449ad0a7, __VMLINUX_SYMBOL_STR(memcmp) },
	{ 0xf9a482f9, __VMLINUX_SYMBOL_STR(msleep) },
	{ 0xce2840e7, __VMLINUX_SYMBOL_STR(irq_set_irq_wake) },
	{ 0xd6b8e852, __VMLINUX_SYMBOL_STR(request_threaded_irq) },
	{ 0x7522f3ba, __VMLINUX_SYMBOL_STR(irq_modify_status) },
	{ 0x16f49030, __VMLINUX_SYMBOL_STR(irq_set_chip_and_handler_name) },
	{ 0x20a789ac, __VMLINUX_SYMBOL_STR(irq_set_chip_data) },
	{ 0xce9c1066, __VMLINUX_SYMBOL_STR(of_irq_to_resource) },
	{ 0xea9536db, __VMLINUX_SYMBOL_STR(handle_level_irq) },
	{ 0xe011b06c, __VMLINUX_SYMBOL_STR(handle_edge_irq) },
	{ 0xaf181a62, __VMLINUX_SYMBOL_STR(irq_find_matching_fwspec) },
	{ 0xbb551ee4, __VMLINUX_SYMBOL_STR(of_irq_find_parent) },
	{ 0x3ce4ca6f, __VMLINUX_SYMBOL_STR(disable_irq) },
	{ 0x27bbf221, __VMLINUX_SYMBOL_STR(disable_irq_nosync) },
	{ 0xfcec0987, __VMLINUX_SYMBOL_STR(enable_irq) },
	{ 0xc1514a3b, __VMLINUX_SYMBOL_STR(free_irq) },
	{ 0x425c362e, __VMLINUX_SYMBOL_STR(pm_relax) },
	{ 0x3c578bac, __VMLINUX_SYMBOL_STR(__wake_up) },
	{ 0x8ddd8aad, __VMLINUX_SYMBOL_STR(schedule_timeout) },
	{ 0x41acaf3c, __VMLINUX_SYMBOL_STR(finish_wait) },
	{ 0x7c580e7, __VMLINUX_SYMBOL_STR(prepare_to_wait_event) },
	{ 0xfe487975, __VMLINUX_SYMBOL_STR(init_wait_entry) },
	{ 0x2df376a3, __VMLINUX_SYMBOL_STR(pm_stay_awake) },
	{ 0xd0d7b9d1, __VMLINUX_SYMBOL_STR(pm_qos_update_request) },
	{ 0xb8202007, __VMLINUX_SYMBOL_STR(msm_cpuidle_get_deep_idle_latency) },
	{ 0xdcb764ad, __VMLINUX_SYMBOL_STR(memset) },
	{ 0xcb2754bb, __VMLINUX_SYMBOL_STR(gpiod_direction_output_raw) },
	{ 0xaf5f825c, __VMLINUX_SYMBOL_STR(gpio_to_desc) },
	{ 0x60ea2d6, __VMLINUX_SYMBOL_STR(kstrtoull) },
	{ 0x85df9b6c, __VMLINUX_SYMBOL_STR(strsep) },
	{ 0x84bc974b, __VMLINUX_SYMBOL_STR(__arch_copy_from_user) },
	{ 0xb35dea8f, __VMLINUX_SYMBOL_STR(__arch_copy_to_user) },
	{ 0x88db9f48, __VMLINUX_SYMBOL_STR(__check_object_size) },
	{ 0x619cb7dd, __VMLINUX_SYMBOL_STR(simple_read_from_buffer) },
	{ 0xa916b694, __VMLINUX_SYMBOL_STR(strnlen) },
	{ 0x28318305, __VMLINUX_SYMBOL_STR(snprintf) },
	{ 0xe2d5255a, __VMLINUX_SYMBOL_STR(strcmp) },
	{ 0x6a5a751a, __VMLINUX_SYMBOL_STR(slim_change_val_element) },
	{ 0x218fe891, __VMLINUX_SYMBOL_STR(slim_request_val_element) },
	{ 0x7ef33997, __VMLINUX_SYMBOL_STR(debugfs_remove_recursive) },
	{ 0x2153ff95, __VMLINUX_SYMBOL_STR(debugfs_create_file) },
	{ 0x5741ee97, __VMLINUX_SYMBOL_STR(debugfs_create_dir) },
	{ 0x4a1bc4d2, __VMLINUX_SYMBOL_STR(slim_add_device) },
	{ 0x1df7fe8f, __VMLINUX_SYMBOL_STR(slim_get_logical_addr) },
	{ 0x2df50b0, __VMLINUX_SYMBOL_STR(jiffies) },
	{ 0x4829a47e, __VMLINUX_SYMBOL_STR(memcpy) },
	{ 0x107e155a, __VMLINUX_SYMBOL_STR(of_find_property) },
	{ 0xb04487ee, __VMLINUX_SYMBOL_STR(slim_get_device_id) },
	{ 0xa896b642, __VMLINUX_SYMBOL_STR(of_property_read_string) },
	{ 0xa1d7119, __VMLINUX_SYMBOL_STR(slim_remove_device) },
	{ 0xe7669d7c, __VMLINUX_SYMBOL_STR(mfd_remove_devices) },
	{ 0x7d36c8fc, __VMLINUX_SYMBOL_STR(device_init_wakeup) },
	{ 0x6a4e27de, __VMLINUX_SYMBOL_STR(mfd_add_devices) },
	{ 0xab5fab1d, __VMLINUX_SYMBOL_STR(regmap_reinit_cache) },
	{ 0xe2eb0ddf, __VMLINUX_SYMBOL_STR(__mutex_init) },
	{ 0x76f385e5, __VMLINUX_SYMBOL_STR(of_match_device) },
	{ 0xc0738423, __VMLINUX_SYMBOL_STR(devm_kmalloc) },
	{ 0xc9288e23, __VMLINUX_SYMBOL_STR(devm_kfree) },
	{ 0x3a65d2a6, __VMLINUX_SYMBOL_STR(slim_driver_unregister) },
	{ 0x1116156, __VMLINUX_SYMBOL_STR(i2c_del_driver) },
	{ 0xabe182da, __VMLINUX_SYMBOL_STR(slim_driver_register) },
	{ 0xeea16081, __VMLINUX_SYMBOL_STR(i2c_register_driver) },
	{ 0x44b1d426, __VMLINUX_SYMBOL_STR(__dynamic_pr_debug) },
	{ 0x1ba3200d, __VMLINUX_SYMBOL_STR(i2c_transfer) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x4014c0ba, __VMLINUX_SYMBOL_STR(slim_bulk_msg_write) },
	{ 0xd2b09ce5, __VMLINUX_SYMBOL_STR(__kmalloc) },
	{ 0x138ed63b, __VMLINUX_SYMBOL_STR(slim_reservemsg_bw) },
	{ 0x45452cf0, __VMLINUX_SYMBOL_STR(___ratelimit) },
	{ 0x15c9d31e, __VMLINUX_SYMBOL_STR(slim_user_msg) },
	{ 0x4aacd53e, __VMLINUX_SYMBOL_STR(mutex_unlock) },
	{ 0x292c6a1a, __VMLINUX_SYMBOL_STR(__dynamic_dev_dbg) },
	{ 0x5e38de65, __VMLINUX_SYMBOL_STR(mutex_lock) },
	{ 0x985558a1, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x3b569d72, __VMLINUX_SYMBOL_STR(regmap_bulk_read) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0xdefb348d, __VMLINUX_SYMBOL_STR(_dev_info) },
	{ 0x433c6216, __VMLINUX_SYMBOL_STR(regmap_read) },
	{ 0x8f678b07, __VMLINUX_SYMBOL_STR(__stack_chk_guard) },
	{ 0x6f4a684f, __VMLINUX_SYMBOL_STR(dev_err) },
	{ 0x12a38747, __VMLINUX_SYMBOL_STR(usleep_range) },
	{ 0x233e820f, __VMLINUX_SYMBOL_STR(regmap_write) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";

MODULE_ALIAS("of:N*T*Cqcom,tavil-i2c");
MODULE_ALIAS("of:N*T*Cqcom,tavil-i2cC*");
MODULE_ALIAS("of:N*T*Cqcom,tasha-i2c-pgd");
MODULE_ALIAS("of:N*T*Cqcom,tasha-i2c-pgdC*");
MODULE_ALIAS("of:N*T*Cqcom,wcd9xxx-i2c");
MODULE_ALIAS("of:N*T*Cqcom,wcd9xxx-i2cC*");
MODULE_ALIAS("i2c:tabla top level");
MODULE_ALIAS("i2c:tabla analog");
MODULE_ALIAS("i2c:tabla digital1");
MODULE_ALIAS("i2c:tabla digital2");
