#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);
MODULE_INFO(name, KBUILD_MODNAME);

__visible struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

#ifdef CONFIG_RETPOLINE
MODULE_INFO(retpoline, "Y");
#endif

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x88f9a64e, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0xb58611fe, __VMLINUX_SYMBOL_STR(complete_and_exit) },
	{ 0xa4421af3, __VMLINUX_SYMBOL_STR(wcd9xxx_slim_write_repeat) },
	{ 0x3b11a732, __VMLINUX_SYMBOL_STR(snd_soc_read) },
	{ 0xaa4e9a2, __VMLINUX_SYMBOL_STR(snd_soc_write) },
	{ 0x5c2f08e5, __VMLINUX_SYMBOL_STR(snd_soc_update_bits) },
	{ 0xe1537255, __VMLINUX_SYMBOL_STR(__list_del_entry_valid) },
	{ 0x68f31cbd, __VMLINUX_SYMBOL_STR(__list_add_valid) },
	{ 0x12a38747, __VMLINUX_SYMBOL_STR(usleep_range) },
	{ 0xf81a01c4, __VMLINUX_SYMBOL_STR(wake_up_process) },
	{ 0xf6b08072, __VMLINUX_SYMBOL_STR(kthread_create_on_node) },
	{ 0xb1c556cf, __VMLINUX_SYMBOL_STR(simple_open) },
	{ 0xe8359364, __VMLINUX_SYMBOL_STR(cal_utils_match_buf_num) },
	{ 0x405c120e, __VMLINUX_SYMBOL_STR(wait_for_completion) },
	{ 0xe2d5255a, __VMLINUX_SYMBOL_STR(strcmp) },
	{ 0xba071f21, __VMLINUX_SYMBOL_STR(wcd9xxx_free_irq) },
	{ 0x2d22edb9, __VMLINUX_SYMBOL_STR(wcd9xxx_request_irq) },
	{ 0xa07a37f0, __VMLINUX_SYMBOL_STR(memchr) },
	{ 0x7f58a719, __VMLINUX_SYMBOL_STR(cal_utils_set_cal) },
	{ 0xf15632d6, __VMLINUX_SYMBOL_STR(cal_utils_dealloc_cal) },
	{ 0xc4d1d388, __VMLINUX_SYMBOL_STR(cal_utils_alloc_cal) },
	{ 0x619cb7dd, __VMLINUX_SYMBOL_STR(simple_read_from_buffer) },
	{ 0x3c578bac, __VMLINUX_SYMBOL_STR(__wake_up) },
	{ 0xe82fd0a6, __VMLINUX_SYMBOL_STR(release_firmware) },
	{ 0x449ad0a7, __VMLINUX_SYMBOL_STR(memcmp) },
	{ 0xb77d33ac, __VMLINUX_SYMBOL_STR(request_firmware) },
	{ 0x6b53e4b7, __VMLINUX_SYMBOL_STR(cal_utils_get_only_cal_block) },
	{ 0x6af6e993, __VMLINUX_SYMBOL_STR(do_ramdump) },
	{ 0xcfc0eb9a, __VMLINUX_SYMBOL_STR(pm_qos_remove_request) },
	{ 0xd2b09ce5, __VMLINUX_SYMBOL_STR(__kmalloc) },
	{ 0xbe69f92a, __VMLINUX_SYMBOL_STR(wait_for_completion_timeout) },
	{ 0xd0d7b9d1, __VMLINUX_SYMBOL_STR(pm_qos_update_request) },
	{ 0xb8202007, __VMLINUX_SYMBOL_STR(msm_cpuidle_get_deep_idle_latency) },
	{ 0xb2532ea5, __VMLINUX_SYMBOL_STR(pm_qos_add_request) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0xdcb764ad, __VMLINUX_SYMBOL_STR(memset) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x226ba57, __VMLINUX_SYMBOL_STR(dma_alloc_from_dev_coherent) },
	{ 0xd703abec, __VMLINUX_SYMBOL_STR(dummy_dma_ops) },
	{ 0xc3012448, __VMLINUX_SYMBOL_STR(arch_setup_dma_ops) },
	{ 0x6a42d5e1, __VMLINUX_SYMBOL_STR(create_ramdump_device) },
	{ 0xbd33b539, __VMLINUX_SYMBOL_STR(kobject_put) },
	{ 0xb2826652, __VMLINUX_SYMBOL_STR(sysfs_create_file_ns) },
	{ 0xe2055b12, __VMLINUX_SYMBOL_STR(kobject_init_and_add) },
	{ 0x94df7e2e, __VMLINUX_SYMBOL_STR(kernel_kobj) },
	{ 0x9f9a21bf, __VMLINUX_SYMBOL_STR(debugfs_remove) },
	{ 0x2153ff95, __VMLINUX_SYMBOL_STR(debugfs_create_file) },
	{ 0x7c32f947, __VMLINUX_SYMBOL_STR(debugfs_create_u32) },
	{ 0x5741ee97, __VMLINUX_SYMBOL_STR(debugfs_create_dir) },
	{ 0x4545910a, __VMLINUX_SYMBOL_STR(cal_utils_create_cal_types) },
	{ 0xda0b8796, __VMLINUX_SYMBOL_STR(snd_info_free_entry) },
	{ 0xaeea3753, __VMLINUX_SYMBOL_STR(snd_info_register) },
	{ 0x53851c5e, __VMLINUX_SYMBOL_STR(snd_info_create_card_entry) },
	{ 0xe2eb0ddf, __VMLINUX_SYMBOL_STR(__mutex_init) },
	{ 0xc2b00af2, __VMLINUX_SYMBOL_STR(__init_waitqueue_head) },
	{ 0x4829a47e, __VMLINUX_SYMBOL_STR(memcpy) },
	{ 0x5792f848, __VMLINUX_SYMBOL_STR(strlcpy) },
	{ 0x28318305, __VMLINUX_SYMBOL_STR(snprintf) },
	{ 0x430c0213, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0xa5db1947, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0x8f678b07, __VMLINUX_SYMBOL_STR(__stack_chk_guard) },
	{ 0x3336736f, __VMLINUX_SYMBOL_STR(complete) },
	{ 0x44b1d426, __VMLINUX_SYMBOL_STR(__dynamic_pr_debug) },
	{ 0x4aacd53e, __VMLINUX_SYMBOL_STR(mutex_unlock) },
	{ 0x5e38de65, __VMLINUX_SYMBOL_STR(mutex_lock) },
	{ 0x292c6a1a, __VMLINUX_SYMBOL_STR(__dynamic_dev_dbg) },
	{ 0x2e0d2f7f, __VMLINUX_SYMBOL_STR(queue_work_on) },
	{ 0x2d3385d3, __VMLINUX_SYMBOL_STR(system_wq) },
	{ 0xdefb348d, __VMLINUX_SYMBOL_STR(_dev_info) },
	{ 0x6f4a684f, __VMLINUX_SYMBOL_STR(dev_err) },
	{ 0x985558a1, __VMLINUX_SYMBOL_STR(printk) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=wcd_core_dlkm,q6_dlkm";

