#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);
MODULE_INFO(name, KBUILD_MODNAME);

__visible struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

#ifdef CONFIG_RETPOLINE
MODULE_INFO(retpoline, "Y");
#endif

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x88f9a64e, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x266f32a7, __VMLINUX_SYMBOL_STR(thermal_zone_device_unregister) },
	{ 0x7681946c, __VMLINUX_SYMBOL_STR(unregister_pm_notifier) },
	{ 0x9cc4f70a, __VMLINUX_SYMBOL_STR(register_pm_notifier) },
	{ 0xb5bb1f1c, __VMLINUX_SYMBOL_STR(thermal_zone_device_register) },
	{ 0xf9a482f9, __VMLINUX_SYMBOL_STR(msleep) },
	{ 0x45452cf0, __VMLINUX_SYMBOL_STR(___ratelimit) },
	{ 0xdbd1083c, __VMLINUX_SYMBOL_STR(__ll_sc___cmpxchg_case_mb_4) },
	{ 0x44b1d426, __VMLINUX_SYMBOL_STR(__dynamic_pr_debug) },
	{ 0xac68ce7b, __VMLINUX_SYMBOL_STR(regcache_cache_only) },
	{ 0xbf7a98fc, __VMLINUX_SYMBOL_STR(snd_soc_dapm_put_volsw) },
	{ 0xeb3e1ef6, __VMLINUX_SYMBOL_STR(snd_soc_dapm_get_volsw) },
	{ 0xd7f99795, __VMLINUX_SYMBOL_STR(snd_soc_info_enum_double) },
	{ 0xa1ecaedd, __VMLINUX_SYMBOL_STR(snd_soc_info_volsw) },
	{ 0xe7364e18, __VMLINUX_SYMBOL_STR(param_ops_int) },
	{ 0x572c9f90, __VMLINUX_SYMBOL_STR(swr_remove_from_group) },
	{ 0x23bc59d4, __VMLINUX_SYMBOL_STR(regmap_multi_reg_write) },
	{ 0xceca285f, __VMLINUX_SYMBOL_STR(swr_slvdev_datapath_control) },
	{ 0x47116bac, __VMLINUX_SYMBOL_STR(swr_disconnect_port) },
	{ 0x5d68ef0e, __VMLINUX_SYMBOL_STR(swr_connect_port) },
	{ 0xaa4e9a2, __VMLINUX_SYMBOL_STR(snd_soc_write) },
	{ 0x75090002, __VMLINUX_SYMBOL_STR(queue_delayed_work_on) },
	{ 0x2d3385d3, __VMLINUX_SYMBOL_STR(system_wq) },
	{ 0x7f02188f, __VMLINUX_SYMBOL_STR(__msecs_to_jiffies) },
	{ 0x6b06fdce, __VMLINUX_SYMBOL_STR(delayed_work_timer_fn) },
	{ 0x5ee52022, __VMLINUX_SYMBOL_STR(init_timer_key) },
	{ 0xa9322de3, __VMLINUX_SYMBOL_STR(snd_soc_add_codec_controls) },
	{ 0x5c2f08e5, __VMLINUX_SYMBOL_STR(snd_soc_update_bits) },
	{ 0x3b11a732, __VMLINUX_SYMBOL_STR(snd_soc_read) },
	{ 0xdcb764ad, __VMLINUX_SYMBOL_STR(memset) },
	{ 0x14868344, __VMLINUX_SYMBOL_STR(swr_write) },
	{ 0xa46f2f1b, __VMLINUX_SYMBOL_STR(kstrtouint) },
	{ 0x85df9b6c, __VMLINUX_SYMBOL_STR(strsep) },
	{ 0x84bc974b, __VMLINUX_SYMBOL_STR(__arch_copy_from_user) },
	{ 0xb35dea8f, __VMLINUX_SYMBOL_STR(__arch_copy_to_user) },
	{ 0x88db9f48, __VMLINUX_SYMBOL_STR(__check_object_size) },
	{ 0xa7095e6c, __VMLINUX_SYMBOL_STR(swr_read) },
	{ 0xa916b694, __VMLINUX_SYMBOL_STR(strnlen) },
	{ 0xe2d5255a, __VMLINUX_SYMBOL_STR(strcmp) },
	{ 0xf3f46ec3, __VMLINUX_SYMBOL_STR(msm_cdc_pinctrl_select_sleep_state) },
	{ 0xcb2754bb, __VMLINUX_SYMBOL_STR(gpiod_direction_output_raw) },
	{ 0xaf5f825c, __VMLINUX_SYMBOL_STR(gpio_to_desc) },
	{ 0x4c5a06e0, __VMLINUX_SYMBOL_STR(msm_cdc_pinctrl_select_active_state) },
	{ 0x4aacd53e, __VMLINUX_SYMBOL_STR(mutex_unlock) },
	{ 0xc5b786d6, __VMLINUX_SYMBOL_STR(regcache_sync) },
	{ 0xdd126190, __VMLINUX_SYMBOL_STR(regcache_mark_dirty) },
	{ 0x5e38de65, __VMLINUX_SYMBOL_STR(mutex_lock) },
	{ 0x64af6747, __VMLINUX_SYMBOL_STR(cancel_delayed_work_sync) },
	{ 0xfe990052, __VMLINUX_SYMBOL_STR(gpio_free) },
	{ 0xe83597a9, __VMLINUX_SYMBOL_STR(snd_soc_unregister_codec) },
	{ 0x7ef33997, __VMLINUX_SYMBOL_STR(debugfs_remove_recursive) },
	{ 0xe2eb0ddf, __VMLINUX_SYMBOL_STR(__mutex_init) },
	{ 0x391bb3bb, __VMLINUX_SYMBOL_STR(snd_soc_register_codec) },
	{ 0xb26331ac, __VMLINUX_SYMBOL_STR(__devm_regmap_init_swr) },
	{ 0x96b012ed, __VMLINUX_SYMBOL_STR(swr_remove_device) },
	{ 0xf09f1dca, __VMLINUX_SYMBOL_STR(swr_get_logical_dev_num) },
	{ 0x12a38747, __VMLINUX_SYMBOL_STR(usleep_range) },
	{ 0x2153ff95, __VMLINUX_SYMBOL_STR(debugfs_create_file) },
	{ 0x5741ee97, __VMLINUX_SYMBOL_STR(debugfs_create_dir) },
	{ 0x47229b5c, __VMLINUX_SYMBOL_STR(gpio_request) },
	{ 0xb885925a, __VMLINUX_SYMBOL_STR(of_get_named_gpio_flags) },
	{ 0x2a380eec, __VMLINUX_SYMBOL_STR(msm_cdc_pinctrl_get_state) },
	{ 0xcee0e5d3, __VMLINUX_SYMBOL_STR(of_parse_phandle) },
	{ 0xc0738423, __VMLINUX_SYMBOL_STR(devm_kmalloc) },
	{ 0x985558a1, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x619cb7dd, __VMLINUX_SYMBOL_STR(simple_read_from_buffer) },
	{ 0xa6bee4ea, __VMLINUX_SYMBOL_STR(swr_driver_unregister) },
	{ 0xf3b80ee0, __VMLINUX_SYMBOL_STR(swr_driver_register) },
	{ 0x6f4a684f, __VMLINUX_SYMBOL_STR(dev_err) },
	{ 0xdc3fcbc9, __VMLINUX_SYMBOL_STR(__sw_hweight8) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0xda0b8796, __VMLINUX_SYMBOL_STR(snd_info_free_entry) },
	{ 0x292c6a1a, __VMLINUX_SYMBOL_STR(__dynamic_dev_dbg) },
	{ 0xaeea3753, __VMLINUX_SYMBOL_STR(snd_info_register) },
	{ 0x53851c5e, __VMLINUX_SYMBOL_STR(snd_info_create_card_entry) },
	{ 0xaebf568a, __VMLINUX_SYMBOL_STR(snd_info_create_subdir) },
	{ 0x28318305, __VMLINUX_SYMBOL_STR(snprintf) },
	{ 0x8f678b07, __VMLINUX_SYMBOL_STR(__stack_chk_guard) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=swr_dlkm,wcd_core_dlkm";

