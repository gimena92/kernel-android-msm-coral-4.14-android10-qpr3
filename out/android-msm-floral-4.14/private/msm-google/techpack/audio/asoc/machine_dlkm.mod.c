#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);
MODULE_INFO(name, KBUILD_MODNAME);

__visible struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

#ifdef CONFIG_RETPOLINE
MODULE_INFO(retpoline, "Y");
#endif

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x88f9a64e, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0xbb1cfe76, __VMLINUX_SYMBOL_STR(snd_soc_info_multi_ext) },
	{ 0xd7f99795, __VMLINUX_SYMBOL_STR(snd_soc_info_enum_double) },
	{ 0x80d284b9, __VMLINUX_SYMBOL_STR(snd_soc_pm_ops) },
	{ 0x2e0d2f7f, __VMLINUX_SYMBOL_STR(queue_work_on) },
	{ 0x2d3385d3, __VMLINUX_SYMBOL_STR(system_wq) },
	{ 0x738e15c6, __VMLINUX_SYMBOL_STR(afe_clear_config) },
	{ 0x6398e3f4, __VMLINUX_SYMBOL_STR(wsa881x_codec_info_create_codec_entry) },
	{ 0x855f6fef, __VMLINUX_SYMBOL_STR(snd_soc_dapm_ignore_suspend) },
	{ 0x138216a7, __VMLINUX_SYMBOL_STR(wsa881x_set_channel_map) },
	{ 0x6029bd14, __VMLINUX_SYMBOL_STR(afe_set_lpass_clock_v2) },
	{ 0x4aacd53e, __VMLINUX_SYMBOL_STR(mutex_unlock) },
	{ 0x5e38de65, __VMLINUX_SYMBOL_STR(mutex_lock) },
	{ 0x44196fa4, __VMLINUX_SYMBOL_STR(snd_soc_dapm_sync) },
	{ 0x12a38747, __VMLINUX_SYMBOL_STR(usleep_range) },
	{ 0xc310b981, __VMLINUX_SYMBOL_STR(strnstr) },
	{ 0xa9322de3, __VMLINUX_SYMBOL_STR(snd_soc_add_codec_controls) },
	{ 0xe922c5ce, __VMLINUX_SYMBOL_STR(snd_soc_codec_set_sysclk) },
	{ 0x84b81bfa, __VMLINUX_SYMBOL_STR(snd_soc_dai_set_fmt) },
	{ 0x62bba544, __VMLINUX_SYMBOL_STR(snd_soc_dai_set_sysclk) },
	{ 0x84b0af12, __VMLINUX_SYMBOL_STR(snd_soc_dai_set_tdm_slot) },
	{ 0xf9a3efb9, __VMLINUX_SYMBOL_STR(__ll_sc_atomic_sub) },
	{ 0x1f7386be, __VMLINUX_SYMBOL_STR(__ll_sc_atomic_add) },
	{ 0xbed1fb33, __VMLINUX_SYMBOL_STR(snd_soc_dai_set_channel_map) },
	{ 0x559508d2, __VMLINUX_SYMBOL_STR(snd_soc_dai_get_channel_map) },
	{ 0xbe3529c0, __VMLINUX_SYMBOL_STR(afe_set_config) },
	{ 0xb2532ea5, __VMLINUX_SYMBOL_STR(pm_qos_add_request) },
	{ 0xae8c4d0c, __VMLINUX_SYMBOL_STR(set_bit) },
	{ 0xcfc0eb9a, __VMLINUX_SYMBOL_STR(pm_qos_remove_request) },
	{ 0x8c8ae46d, __VMLINUX_SYMBOL_STR(pm_qos_request_active) },
	{ 0x4c5a06e0, __VMLINUX_SYMBOL_STR(msm_cdc_pinctrl_select_active_state) },
	{ 0xf3f46ec3, __VMLINUX_SYMBOL_STR(msm_cdc_pinctrl_select_sleep_state) },
	{ 0x2a380eec, __VMLINUX_SYMBOL_STR(msm_cdc_pinctrl_get_state) },
	{ 0x9673e3cb, __VMLINUX_SYMBOL_STR(fsa4480_switch_event) },
	{ 0x7d1e9321, __VMLINUX_SYMBOL_STR(snd_soc_card_change_online_state) },
	{ 0xf9a482f9, __VMLINUX_SYMBOL_STR(msleep) },
	{ 0x7e52e977, __VMLINUX_SYMBOL_STR(q6core_is_adsp_ready) },
	{ 0x5d250fe1, __VMLINUX_SYMBOL_STR(snd_card_is_online_state) },
	{ 0x2df50b0, __VMLINUX_SYMBOL_STR(jiffies) },
	{ 0xc5656b4d, __VMLINUX_SYMBOL_STR(audio_notifier_deregister) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0x44b1d426, __VMLINUX_SYMBOL_STR(__dynamic_pr_debug) },
	{ 0x5f986d19, __VMLINUX_SYMBOL_STR(pinctrl_select_state) },
	{ 0xa7fe7810, __VMLINUX_SYMBOL_STR(audio_notifier_register) },
	{ 0xe2eb0ddf, __VMLINUX_SYMBOL_STR(__mutex_init) },
	{ 0x28318305, __VMLINUX_SYMBOL_STR(snprintf) },
	{ 0x98cf60b3, __VMLINUX_SYMBOL_STR(strlen) },
	{ 0x19e083d7, __VMLINUX_SYMBOL_STR(pinctrl_lookup_state) },
	{ 0x4e4540de, __VMLINUX_SYMBOL_STR(devm_pinctrl_get) },
	{ 0xa896b642, __VMLINUX_SYMBOL_STR(of_property_read_string) },
	{ 0xdd64e7ea, __VMLINUX_SYMBOL_STR(soc_find_component) },
	{ 0x59e7eb75, __VMLINUX_SYMBOL_STR(of_property_read_string_helper) },
	{ 0x292c6a1a, __VMLINUX_SYMBOL_STR(__dynamic_dev_dbg) },
	{ 0xf1784e2a, __VMLINUX_SYMBOL_STR(of_platform_populate) },
	{ 0x2dcfaa88, __VMLINUX_SYMBOL_STR(devm_snd_soc_register_card) },
	{ 0xdefb348d, __VMLINUX_SYMBOL_STR(_dev_info) },
	{ 0x41d7b718, __VMLINUX_SYMBOL_STR(dev_warn) },
	{ 0xb5f8070c, __VMLINUX_SYMBOL_STR(of_count_phandle_with_args) },
	{ 0x28da7483, __VMLINUX_SYMBOL_STR(of_property_read_variable_u32_array) },
	{ 0xcee0e5d3, __VMLINUX_SYMBOL_STR(of_parse_phandle) },
	{ 0x1dde0b65, __VMLINUX_SYMBOL_STR(of_property_match_string) },
	{ 0xc9288e23, __VMLINUX_SYMBOL_STR(devm_kfree) },
	{ 0xa2033a72, __VMLINUX_SYMBOL_STR(devm_pinctrl_put) },
	{ 0x4a005bff, __VMLINUX_SYMBOL_STR(snd_soc_of_parse_audio_routing) },
	{ 0xbef1208e, __VMLINUX_SYMBOL_STR(snd_soc_of_parse_card_name) },
	{ 0x107e155a, __VMLINUX_SYMBOL_STR(of_find_property) },
	{ 0x4829a47e, __VMLINUX_SYMBOL_STR(memcpy) },
	{ 0x985558a1, __VMLINUX_SYMBOL_STR(printk) },
	{ 0xe2d5255a, __VMLINUX_SYMBOL_STR(strcmp) },
	{ 0xea56c475, __VMLINUX_SYMBOL_STR(of_match_node) },
	{ 0xc0738423, __VMLINUX_SYMBOL_STR(devm_kmalloc) },
	{ 0x8f678b07, __VMLINUX_SYMBOL_STR(__stack_chk_guard) },
	{ 0x3fc79f5c, __VMLINUX_SYMBOL_STR(platform_driver_unregister) },
	{ 0xefcc7c9, __VMLINUX_SYMBOL_STR(__platform_driver_register) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x6f4a684f, __VMLINUX_SYMBOL_STR(dev_err) },
	{ 0x9e53176e, __VMLINUX_SYMBOL_STR(tavil_mbhc_hs_detect) },
	{ 0x430c0213, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0xa5db1947, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0xfbe32421, __VMLINUX_SYMBOL_STR(snd_soc_get_pcm_runtime) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=q6_dlkm,wsa881x_dlkm,wcd_core_dlkm,q6_notifier_dlkm,wcd934x_dlkm";

MODULE_ALIAS("of:N*T*Cqcom,sm8150-asoc-snd-stub");
MODULE_ALIAS("of:N*T*Cqcom,sm8150-asoc-snd-stubC*");
MODULE_ALIAS("of:N*T*Cqcom,sm8150-asoc-snd");
MODULE_ALIAS("of:N*T*Cqcom,sm8150-asoc-sndC*");
