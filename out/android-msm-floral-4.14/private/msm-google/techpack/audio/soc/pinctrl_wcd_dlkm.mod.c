#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);
MODULE_INFO(name, KBUILD_MODNAME);

__visible struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

#ifdef CONFIG_RETPOLINE
MODULE_INFO(retpoline, "Y");
#endif

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x88f9a64e, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0xf10acfef, __VMLINUX_SYMBOL_STR(pinctrl_utils_free_map) },
	{ 0x433c6216, __VMLINUX_SYMBOL_STR(regmap_read) },
	{ 0xd2625f4e, __VMLINUX_SYMBOL_STR(gpiochip_get_data) },
	{ 0x50e85e4d, __VMLINUX_SYMBOL_STR(regmap_update_bits_base) },
	{ 0x292c6a1a, __VMLINUX_SYMBOL_STR(__dynamic_dev_dbg) },
	{ 0x29187daa, __VMLINUX_SYMBOL_STR(pinctrl_dev_get_drvdata) },
	{ 0x85705943, __VMLINUX_SYMBOL_STR(pinconf_generic_dt_node_to_map) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0xc9288e23, __VMLINUX_SYMBOL_STR(devm_kfree) },
	{ 0x92c996d1, __VMLINUX_SYMBOL_STR(gpiochip_remove) },
	{ 0xc1461500, __VMLINUX_SYMBOL_STR(gpiochip_add_pin_range) },
	{ 0xa7db7ff0, __VMLINUX_SYMBOL_STR(gpiochip_add_data) },
	{ 0x3858464b, __VMLINUX_SYMBOL_STR(devm_pinctrl_register) },
	{ 0x4829a47e, __VMLINUX_SYMBOL_STR(memcpy) },
	{ 0x6f4a684f, __VMLINUX_SYMBOL_STR(dev_err) },
	{ 0x28318305, __VMLINUX_SYMBOL_STR(snprintf) },
	{ 0xc14187d5, __VMLINUX_SYMBOL_STR(dev_get_regmap) },
	{ 0xc0738423, __VMLINUX_SYMBOL_STR(devm_kmalloc) },
	{ 0x28da7483, __VMLINUX_SYMBOL_STR(of_property_read_variable_u32_array) },
	{ 0x8f678b07, __VMLINUX_SYMBOL_STR(__stack_chk_guard) },
	{ 0x3fc79f5c, __VMLINUX_SYMBOL_STR(platform_driver_unregister) },
	{ 0xefcc7c9, __VMLINUX_SYMBOL_STR(__platform_driver_register) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";

MODULE_ALIAS("of:N*T*Cqcom,wcd-pinctrl");
MODULE_ALIAS("of:N*T*Cqcom,wcd-pinctrlC*");
