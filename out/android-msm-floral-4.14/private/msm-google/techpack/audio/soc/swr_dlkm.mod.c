#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);
MODULE_INFO(name, KBUILD_MODNAME);

__visible struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

#ifdef CONFIG_RETPOLINE
MODULE_INFO(retpoline, "Y");
#endif

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x88f9a64e, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0xa3f1b5f0, __VMLINUX_SYMBOL_STR(pm_generic_resume) },
	{ 0x9d84df6e, __VMLINUX_SYMBOL_STR(pm_generic_suspend) },
	{ 0x9fdf3c53, __VMLINUX_SYMBOL_STR(bus_unregister) },
	{ 0x1767f8da, __VMLINUX_SYMBOL_STR(bus_register) },
	{ 0xe2d5255a, __VMLINUX_SYMBOL_STR(strcmp) },
	{ 0x44b1d426, __VMLINUX_SYMBOL_STR(__dynamic_pr_debug) },
	{ 0xe2eb0ddf, __VMLINUX_SYMBOL_STR(__mutex_init) },
	{ 0xeaae1608, __VMLINUX_SYMBOL_STR(idr_alloc_cmn) },
	{ 0x5c54dbd9, __VMLINUX_SYMBOL_STR(of_alias_get_id) },
	{ 0x190c5dbe, __VMLINUX_SYMBOL_STR(device_unregister) },
	{ 0xce398ad, __VMLINUX_SYMBOL_STR(device_for_each_child) },
	{ 0x37e77782, __VMLINUX_SYMBOL_STR(radix_tree_delete_item) },
	{ 0x92a6f160, __VMLINUX_SYMBOL_STR(radix_tree_lookup) },
	{ 0xa305610d, __VMLINUX_SYMBOL_STR(driver_unregister) },
	{ 0x58365e75, __VMLINUX_SYMBOL_STR(driver_register) },
	{ 0x85f5e2aa, __VMLINUX_SYMBOL_STR(krealloc) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0x53a0489d, __VMLINUX_SYMBOL_STR(of_property_read_u64) },
	{ 0x76b7d17f, __VMLINUX_SYMBOL_STR(of_modalias_node) },
	{ 0x4aa27150, __VMLINUX_SYMBOL_STR(of_get_next_available_child) },
	{ 0x8f678b07, __VMLINUX_SYMBOL_STR(__stack_chk_guard) },
	{ 0x292c6a1a, __VMLINUX_SYMBOL_STR(__dynamic_dev_dbg) },
	{ 0x985558a1, __VMLINUX_SYMBOL_STR(printk) },
	{ 0xa56dc984, __VMLINUX_SYMBOL_STR(put_device) },
	{ 0xcaef2553, __VMLINUX_SYMBOL_STR(device_register) },
	{ 0x6982b0ef, __VMLINUX_SYMBOL_STR(dev_set_name) },
	{ 0x4aacd53e, __VMLINUX_SYMBOL_STR(mutex_unlock) },
	{ 0x68f31cbd, __VMLINUX_SYMBOL_STR(__list_add_valid) },
	{ 0x5e38de65, __VMLINUX_SYMBOL_STR(mutex_lock) },
	{ 0x5792f848, __VMLINUX_SYMBOL_STR(strlcpy) },
	{ 0x430c0213, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0xa5db1947, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0xe7053c29, __VMLINUX_SYMBOL_STR(get_device) },
	{ 0xe1537255, __VMLINUX_SYMBOL_STR(__list_del_entry_valid) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x6f4a684f, __VMLINUX_SYMBOL_STR(dev_err) },
	{ 0xd2b09ce5, __VMLINUX_SYMBOL_STR(__kmalloc) },
	{ 0xc14187d5, __VMLINUX_SYMBOL_STR(dev_get_regmap) },
	{ 0x1cf816a0, __VMLINUX_SYMBOL_STR(__devm_regmap_init) },
	{ 0x3a267c0c, __VMLINUX_SYMBOL_STR(__regmap_init) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";

