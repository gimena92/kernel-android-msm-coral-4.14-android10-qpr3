#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);
MODULE_INFO(name, KBUILD_MODNAME);

__visible struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

#ifdef CONFIG_RETPOLINE
MODULE_INFO(retpoline, "Y");
#endif

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x88f9a64e, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x292c6a1a, __VMLINUX_SYMBOL_STR(__dynamic_dev_dbg) },
	{ 0xdefb348d, __VMLINUX_SYMBOL_STR(_dev_info) },
	{ 0x62f1bfdf, __VMLINUX_SYMBOL_STR(unregister_rpmsg_driver) },
	{ 0xaf92cb7a, __VMLINUX_SYMBOL_STR(__register_rpmsg_driver) },
	{ 0xeae3dfd6, __VMLINUX_SYMBOL_STR(__const_udelay) },
	{ 0xbd29c66c, __VMLINUX_SYMBOL_STR(rpmsg_trysend) },
	{ 0xc5656b4d, __VMLINUX_SYMBOL_STR(audio_notifier_deregister) },
	{ 0xa7fe7810, __VMLINUX_SYMBOL_STR(audio_notifier_register) },
	{ 0x84bc974b, __VMLINUX_SYMBOL_STR(__arch_copy_from_user) },
	{ 0xdcb764ad, __VMLINUX_SYMBOL_STR(memset) },
	{ 0x2d3385d3, __VMLINUX_SYMBOL_STR(system_wq) },
	{ 0x3c578bac, __VMLINUX_SYMBOL_STR(__wake_up) },
	{ 0x6f4a684f, __VMLINUX_SYMBOL_STR(dev_err) },
	{ 0xf1784e2a, __VMLINUX_SYMBOL_STR(of_platform_populate) },
	{ 0x9f9a21bf, __VMLINUX_SYMBOL_STR(debugfs_remove) },
	{ 0x8c03d20c, __VMLINUX_SYMBOL_STR(destroy_workqueue) },
	{ 0x42160169, __VMLINUX_SYMBOL_STR(flush_workqueue) },
	{ 0x64c6cac5, __VMLINUX_SYMBOL_STR(of_platform_depopulate) },
	{ 0x2153ff95, __VMLINUX_SYMBOL_STR(debugfs_create_file) },
	{ 0xa896b642, __VMLINUX_SYMBOL_STR(of_property_read_string) },
	{ 0xf33847d3, __VMLINUX_SYMBOL_STR(_raw_spin_unlock) },
	{ 0x5cd885d5, __VMLINUX_SYMBOL_STR(_raw_spin_lock) },
	{ 0xe0b86220, __VMLINUX_SYMBOL_STR(ipc_log_context_create) },
	{ 0x43a53735, __VMLINUX_SYMBOL_STR(__alloc_workqueue_key) },
	{ 0xe2eb0ddf, __VMLINUX_SYMBOL_STR(__mutex_init) },
	{ 0xc0738423, __VMLINUX_SYMBOL_STR(devm_kmalloc) },
	{ 0xc2b00af2, __VMLINUX_SYMBOL_STR(__init_waitqueue_head) },
	{ 0x3fc79f5c, __VMLINUX_SYMBOL_STR(platform_driver_unregister) },
	{ 0xefcc7c9, __VMLINUX_SYMBOL_STR(__platform_driver_register) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x2e0d2f7f, __VMLINUX_SYMBOL_STR(queue_work_on) },
	{ 0x430c0213, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0xa5db1947, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0x8ddd8aad, __VMLINUX_SYMBOL_STR(schedule_timeout) },
	{ 0x41acaf3c, __VMLINUX_SYMBOL_STR(finish_wait) },
	{ 0x7c580e7, __VMLINUX_SYMBOL_STR(prepare_to_wait_event) },
	{ 0xfe487975, __VMLINUX_SYMBOL_STR(init_wait_entry) },
	{ 0xe2d5255a, __VMLINUX_SYMBOL_STR(strcmp) },
	{ 0x8f678b07, __VMLINUX_SYMBOL_STR(__stack_chk_guard) },
	{ 0x97fdbab9, __VMLINUX_SYMBOL_STR(_raw_spin_unlock_irqrestore) },
	{ 0x8b83a1e8, __VMLINUX_SYMBOL_STR(ipc_log_string) },
	{ 0x96220280, __VMLINUX_SYMBOL_STR(_raw_spin_lock_irqsave) },
	{ 0x45452cf0, __VMLINUX_SYMBOL_STR(___ratelimit) },
	{ 0x4aacd53e, __VMLINUX_SYMBOL_STR(mutex_unlock) },
	{ 0x985558a1, __VMLINUX_SYMBOL_STR(printk) },
	{ 0x41fac243, __VMLINUX_SYMBOL_STR(subsystem_get) },
	{ 0x5e38de65, __VMLINUX_SYMBOL_STR(mutex_lock) },
	{ 0x44b1d426, __VMLINUX_SYMBOL_STR(__dynamic_pr_debug) },
	{ 0xdbd1083c, __VMLINUX_SYMBOL_STR(__ll_sc___cmpxchg_case_mb_4) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=q6_notifier_dlkm";

MODULE_ALIAS("of:N*T*Cqcom,msm-audio-apr");
MODULE_ALIAS("of:N*T*Cqcom,msm-audio-aprC*");
