#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);
MODULE_INFO(name, KBUILD_MODNAME);

__visible struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

#ifdef CONFIG_RETPOLINE
MODULE_INFO(retpoline, "Y");
#endif

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x88f9a64e, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0xb49633c, __VMLINUX_SYMBOL_STR(irq_to_desc) },
	{ 0x6af6e993, __VMLINUX_SYMBOL_STR(do_ramdump) },
	{ 0x815588a6, __VMLINUX_SYMBOL_STR(clk_enable) },
	{ 0x7c9a7371, __VMLINUX_SYMBOL_STR(clk_prepare) },
	{ 0xe8b9ae78, __VMLINUX_SYMBOL_STR(devm_clk_get) },
	{ 0xe26c1864, __VMLINUX_SYMBOL_STR(regulator_set_load) },
	{ 0x67beed4, __VMLINUX_SYMBOL_STR(regulator_set_voltage) },
	{ 0x369e3f0, __VMLINUX_SYMBOL_STR(regulator_count_voltages) },
	{ 0x6cde8e6d, __VMLINUX_SYMBOL_STR(regulator_disable) },
	{ 0x1154081c, __VMLINUX_SYMBOL_STR(regulator_enable) },
	{ 0x4e24ab2, __VMLINUX_SYMBOL_STR(devm_regulator_get) },
	{ 0xc499ae1e, __VMLINUX_SYMBOL_STR(kstrdup) },
	{ 0x8717e08f, __VMLINUX_SYMBOL_STR(pci_dev_put) },
	{ 0xe1537255, __VMLINUX_SYMBOL_STR(__list_del_entry_valid) },
	{ 0x2037e9ae, __VMLINUX_SYMBOL_STR(msm_pcie_deregister_event) },
	{ 0x68f31cbd, __VMLINUX_SYMBOL_STR(__list_add_valid) },
	{ 0xc89939aa, __VMLINUX_SYMBOL_STR(msm_pcie_register_event) },
	{ 0xfe990052, __VMLINUX_SYMBOL_STR(gpio_free) },
	{ 0xae8c4d0c, __VMLINUX_SYMBOL_STR(set_bit) },
	{ 0x32e79710, __VMLINUX_SYMBOL_STR(cpumask_next) },
	{ 0x17de3d5, __VMLINUX_SYMBOL_STR(nr_cpu_ids) },
	{ 0x44468156, __VMLINUX_SYMBOL_STR(__cpu_possible_mask) },
	{ 0x33f0768c, __VMLINUX_SYMBOL_STR(cpufreq_quick_get_max) },
	{ 0x3424b043, __VMLINUX_SYMBOL_STR(subsys_unregister) },
	{ 0x69172e7f, __VMLINUX_SYMBOL_STR(destroy_ramdump_device) },
	{ 0x6a42d5e1, __VMLINUX_SYMBOL_STR(create_ramdump_device) },
	{ 0x7ac1f2a7, __VMLINUX_SYMBOL_STR(msm_dump_data_register) },
	{ 0xfadf2436, __VMLINUX_SYMBOL_STR(memstart_addr) },
	{ 0x228f4555, __VMLINUX_SYMBOL_STR(kimage_voffset) },
	{ 0xd2b09ce5, __VMLINUX_SYMBOL_STR(__kmalloc) },
	{ 0xd6d613df, __VMLINUX_SYMBOL_STR(subsys_register) },
	{ 0x4998bbce, __VMLINUX_SYMBOL_STR(pci_get_device) },
	{ 0x8dda4754, __VMLINUX_SYMBOL_STR(msm_pcie_enumerate) },
	{ 0xf9a482f9, __VMLINUX_SYMBOL_STR(msleep) },
	{ 0xcb2754bb, __VMLINUX_SYMBOL_STR(gpiod_direction_output_raw) },
	{ 0xaf5f825c, __VMLINUX_SYMBOL_STR(gpio_to_desc) },
	{ 0x47229b5c, __VMLINUX_SYMBOL_STR(gpio_request) },
	{ 0x7a271d35, __VMLINUX_SYMBOL_STR(devm_clk_put) },
	{ 0x1dde0b65, __VMLINUX_SYMBOL_STR(of_property_match_string) },
	{ 0xba651b32, __VMLINUX_SYMBOL_STR(devm_regulator_put) },
	{ 0x81701a84, __VMLINUX_SYMBOL_STR(msm_bus_cl_get_pdata) },
	{ 0x107e155a, __VMLINUX_SYMBOL_STR(of_find_property) },
	{ 0x28da7483, __VMLINUX_SYMBOL_STR(of_property_read_variable_u32_array) },
	{ 0xcee0e5d3, __VMLINUX_SYMBOL_STR(of_parse_phandle) },
	{ 0xb885925a, __VMLINUX_SYMBOL_STR(of_get_named_gpio_flags) },
	{ 0xc0738423, __VMLINUX_SYMBOL_STR(devm_kmalloc) },
	{ 0x3a0d61bc, __VMLINUX_SYMBOL_STR(subsystem_put) },
	{ 0x41fac243, __VMLINUX_SYMBOL_STR(subsystem_get) },
	{ 0x8b2627a6, __VMLINUX_SYMBOL_STR(subsystem_restart_dev) },
	{ 0x7f2a6c83, __VMLINUX_SYMBOL_STR(subsys_set_crash_status) },
	{ 0x5792f848, __VMLINUX_SYMBOL_STR(strlcpy) },
	{ 0xb077e70a, __VMLINUX_SYMBOL_STR(clk_unprepare) },
	{ 0xb6e6d99d, __VMLINUX_SYMBOL_STR(clk_disable) },
	{ 0x53ca0d03, __VMLINUX_SYMBOL_STR(arm_iommu_detach_device) },
	{ 0xc3ba06e9, __VMLINUX_SYMBOL_STR(pci_set_master) },
	{ 0x8c7e3ebb, __VMLINUX_SYMBOL_STR(pci_enable_device) },
	{ 0x8719627e, __VMLINUX_SYMBOL_STR(pci_restore_state) },
	{ 0x59c277e7, __VMLINUX_SYMBOL_STR(pci_load_saved_state) },
	{ 0x44b1d426, __VMLINUX_SYMBOL_STR(__dynamic_pr_debug) },
	{ 0xfb64dd2c, __VMLINUX_SYMBOL_STR(msm_pcie_pm_control) },
	{ 0xc19054d9, __VMLINUX_SYMBOL_STR(pci_set_power_state) },
	{ 0x858700a1, __VMLINUX_SYMBOL_STR(pci_store_saved_state) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x983288fb, __VMLINUX_SYMBOL_STR(pci_save_state) },
	{ 0x9184e29a, __VMLINUX_SYMBOL_STR(pci_disable_device) },
	{ 0x41d7b718, __VMLINUX_SYMBOL_STR(dev_warn) },
	{ 0xa0208e02, __VMLINUX_SYMBOL_STR(irq_set_affinity_hint) },
	{ 0x7522f3ba, __VMLINUX_SYMBOL_STR(irq_modify_status) },
	{ 0x78f062cb, __VMLINUX_SYMBOL_STR(msm_bus_scale_client_update_request) },
	{ 0x292c6a1a, __VMLINUX_SYMBOL_STR(__dynamic_dev_dbg) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0xcf8cc5ee, __VMLINUX_SYMBOL_STR(msm_bus_scale_unregister_client) },
	{ 0x8b966278, __VMLINUX_SYMBOL_STR(arm_iommu_attach_device) },
	{ 0xc3012448, __VMLINUX_SYMBOL_STR(arch_setup_dma_ops) },
	{ 0xdcb764ad, __VMLINUX_SYMBOL_STR(memset) },
	{ 0x985558a1, __VMLINUX_SYMBOL_STR(printk) },
	{ 0xb0888f81, __VMLINUX_SYMBOL_STR(arm_iommu_release_mapping) },
	{ 0x6f4a684f, __VMLINUX_SYMBOL_STR(dev_err) },
	{ 0xbf58e71f, __VMLINUX_SYMBOL_STR(iommu_domain_set_attr) },
	{ 0x64c464ce, __VMLINUX_SYMBOL_STR(arm_iommu_create_mapping) },
	{ 0x78bfce80, __VMLINUX_SYMBOL_STR(platform_bus_type) },
	{ 0xdefb348d, __VMLINUX_SYMBOL_STR(_dev_info) },
	{ 0x275d51bf, __VMLINUX_SYMBOL_STR(msm_bus_scale_register_client) },
	{ 0x8f678b07, __VMLINUX_SYMBOL_STR(__stack_chk_guard) },
	{ 0x3fc79f5c, __VMLINUX_SYMBOL_STR(platform_driver_unregister) },
	{ 0xefcc7c9, __VMLINUX_SYMBOL_STR(__platform_driver_register) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";

