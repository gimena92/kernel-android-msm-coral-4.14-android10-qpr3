#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);
MODULE_INFO(name, KBUILD_MODNAME);

__visible struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

#ifdef CONFIG_RETPOLINE
MODULE_INFO(retpoline, "Y");
#endif

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x88f9a64e, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x2098f47b, __VMLINUX_SYMBOL_STR(vb2_ioctl_streamoff) },
	{ 0x576915c9, __VMLINUX_SYMBOL_STR(vb2_ioctl_streamon) },
	{ 0xbdada781, __VMLINUX_SYMBOL_STR(vb2_ioctl_create_bufs) },
	{ 0x38fe373a, __VMLINUX_SYMBOL_STR(vb2_ioctl_dqbuf) },
	{ 0xf8bbad4d, __VMLINUX_SYMBOL_STR(vb2_ioctl_expbuf) },
	{ 0x934bdb3, __VMLINUX_SYMBOL_STR(vb2_ioctl_qbuf) },
	{ 0x6821d9ae, __VMLINUX_SYMBOL_STR(vb2_ioctl_querybuf) },
	{ 0xc4676652, __VMLINUX_SYMBOL_STR(vb2_ioctl_reqbufs) },
	{ 0x860d7d61, __VMLINUX_SYMBOL_STR(vb2_fop_release) },
	{ 0xca0cde3e, __VMLINUX_SYMBOL_STR(v4l2_fh_open) },
	{ 0xac63e6be, __VMLINUX_SYMBOL_STR(vb2_fop_mmap) },
	{ 0x3319325, __VMLINUX_SYMBOL_STR(video_ioctl2) },
	{ 0x98bad588, __VMLINUX_SYMBOL_STR(vb2_fop_poll) },
	{ 0x4b0272fc, __VMLINUX_SYMBOL_STR(vb2_fop_read) },
	{ 0xaa124c21, __VMLINUX_SYMBOL_STR(vb2_ops_wait_finish) },
	{ 0x5799b012, __VMLINUX_SYMBOL_STR(vb2_ops_wait_prepare) },
	{ 0xad3923e2, __VMLINUX_SYMBOL_STR(video_device_release_empty) },
	{ 0xc9fe6153, __VMLINUX_SYMBOL_STR(vb2_vmalloc_memops) },
	{ 0xc56edc04, __VMLINUX_SYMBOL_STR(video_devdata) },
	{ 0x68f31cbd, __VMLINUX_SYMBOL_STR(__list_add_valid) },
	{ 0x448c4975, __VMLINUX_SYMBOL_STR(video_unregister_device) },
	{ 0xfd4778c0, __VMLINUX_SYMBOL_STR(v4l2_device_unregister) },
	{ 0x321c6e7c, __VMLINUX_SYMBOL_STR(video_device_release) },
	{ 0xf0170552, __VMLINUX_SYMBOL_STR(__video_register_device) },
	{ 0x63844f8b, __VMLINUX_SYMBOL_STR(vb2_queue_init) },
	{ 0xe2eb0ddf, __VMLINUX_SYMBOL_STR(__mutex_init) },
	{ 0x5d55d0a7, __VMLINUX_SYMBOL_STR(v4l2_device_register) },
	{ 0x5792f848, __VMLINUX_SYMBOL_STR(strlcpy) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0x669f7a90, __VMLINUX_SYMBOL_STR(vb2_buffer_done) },
	{ 0x6f4a684f, __VMLINUX_SYMBOL_STR(dev_err) },
	{ 0x41d7b718, __VMLINUX_SYMBOL_STR(dev_warn) },
	{ 0x4829a47e, __VMLINUX_SYMBOL_STR(memcpy) },
	{ 0x4f382030, __VMLINUX_SYMBOL_STR(vb2_plane_vaddr) },
	{ 0xe1537255, __VMLINUX_SYMBOL_STR(__list_del_entry_valid) },
	{ 0xf33847d3, __VMLINUX_SYMBOL_STR(_raw_spin_unlock) },
	{ 0x5cd885d5, __VMLINUX_SYMBOL_STR(_raw_spin_lock) },
	{ 0x8f678b07, __VMLINUX_SYMBOL_STR(__stack_chk_guard) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=videobuf2-vmalloc";

