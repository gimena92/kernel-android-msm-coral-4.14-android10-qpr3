#include <linux/module.h>
#include <linux/vermagic.h>
#include <linux/compiler.h>

MODULE_INFO(vermagic, VERMAGIC_STRING);
MODULE_INFO(name, KBUILD_MODNAME);

__visible struct module __this_module
__attribute__((section(".gnu.linkonce.this_module"))) = {
	.name = KBUILD_MODNAME,
	.init = init_module,
#ifdef CONFIG_MODULE_UNLOAD
	.exit = cleanup_module,
#endif
	.arch = MODULE_ARCH_INIT,
};

MODULE_INFO(intree, "Y");

#ifdef CONFIG_RETPOLINE
MODULE_INFO(retpoline, "Y");
#endif

static const struct modversion_info ____versions[]
__used
__attribute__((section("__versions"))) = {
	{ 0x88f9a64e, __VMLINUX_SYMBOL_STR(module_layout) },
	{ 0x99af12d3, __VMLINUX_SYMBOL_STR(kmem_cache_destroy) },
	{ 0x93add0dd, __VMLINUX_SYMBOL_STR(kmem_cache_create) },
	{ 0x675bda12, __VMLINUX_SYMBOL_STR(kmem_cache_free) },
	{ 0xa7f4d0d0, __VMLINUX_SYMBOL_STR(kmem_cache_alloc) },
	{ 0xd2b09ce5, __VMLINUX_SYMBOL_STR(__kmalloc) },
	{ 0xec2ac905, __VMLINUX_SYMBOL_STR(__ll_sc_atomic_sub_return) },
	{ 0x1f7386be, __VMLINUX_SYMBOL_STR(__ll_sc_atomic_add) },
	{ 0xbfcca0ba, __VMLINUX_SYMBOL_STR(refcount_sub_and_test) },
	{ 0x11da9f1a, __VMLINUX_SYMBOL_STR(refcount_dec_and_test) },
	{ 0x3b45538e, __VMLINUX_SYMBOL_STR(refcount_add_not_zero) },
	{ 0xb2e5e67f, __VMLINUX_SYMBOL_STR(refcount_inc_not_zero) },
	{ 0xba54f596, __VMLINUX_SYMBOL_STR(refcount_add) },
	{ 0x4041c710, __VMLINUX_SYMBOL_STR(refcount_inc) },
	{ 0x61f132b1, __VMLINUX_SYMBOL_STR(refcount_dec) },
	{ 0xb35dea8f, __VMLINUX_SYMBOL_STR(__arch_copy_to_user) },
	{ 0x5b56860c, __VMLINUX_SYMBOL_STR(vm_munmap) },
	{ 0x7f4f90e6, __VMLINUX_SYMBOL_STR(access_process_vm) },
	{ 0x3ec00946, __VMLINUX_SYMBOL_STR(vm_mmap) },
	{ 0x999e8297, __VMLINUX_SYMBOL_STR(vfree) },
	{ 0xd6ee688f, __VMLINUX_SYMBOL_STR(vmalloc) },
	{ 0x5186ca4, __VMLINUX_SYMBOL_STR(flush_icache_range) },
	{ 0x4829a47e, __VMLINUX_SYMBOL_STR(memcpy) },
	{ 0x37a0cba, __VMLINUX_SYMBOL_STR(kfree) },
	{ 0x430c0213, __VMLINUX_SYMBOL_STR(kmem_cache_alloc_trace) },
	{ 0xa5db1947, __VMLINUX_SYMBOL_STR(kmalloc_caches) },
	{ 0x7236d720, __VMLINUX_SYMBOL_STR(force_sig) },
	{ 0xae8c4d0c, __VMLINUX_SYMBOL_STR(set_bit) },
	{ 0xe1537255, __VMLINUX_SYMBOL_STR(__list_del_entry_valid) },
	{ 0x68f31cbd, __VMLINUX_SYMBOL_STR(__list_add_valid) },
	{ 0x1000e51, __VMLINUX_SYMBOL_STR(schedule) },
	{ 0x5cd885d5, __VMLINUX_SYMBOL_STR(_raw_spin_lock) },
	{ 0xf0fdf6cb, __VMLINUX_SYMBOL_STR(__stack_chk_fail) },
	{ 0x8f678b07, __VMLINUX_SYMBOL_STR(__stack_chk_guard) },
	{ 0x7c1372e8, __VMLINUX_SYMBOL_STR(panic) },
	{ 0x60d3d38c, __VMLINUX_SYMBOL_STR(param_ops_charp) },
	{ 0xe7364e18, __VMLINUX_SYMBOL_STR(param_ops_int) },
	{ 0x1bb8ed41, __VMLINUX_SYMBOL_STR(generic_file_llseek) },
	{ 0x7ef33997, __VMLINUX_SYMBOL_STR(debugfs_remove_recursive) },
	{ 0x2153ff95, __VMLINUX_SYMBOL_STR(debugfs_create_file) },
	{ 0x5741ee97, __VMLINUX_SYMBOL_STR(debugfs_create_dir) },
	{ 0xdcb764ad, __VMLINUX_SYMBOL_STR(memset) },
	{ 0x985558a1, __VMLINUX_SYMBOL_STR(printk) },
	{ 0xe2d5255a, __VMLINUX_SYMBOL_STR(strcmp) },
	{ 0x77bc13a0, __VMLINUX_SYMBOL_STR(strim) },
	{ 0x84bc974b, __VMLINUX_SYMBOL_STR(__arch_copy_from_user) },
	{ 0x88db9f48, __VMLINUX_SYMBOL_STR(__check_object_size) },
	{ 0x4302d0eb, __VMLINUX_SYMBOL_STR(free_pages) },
	{ 0x619cb7dd, __VMLINUX_SYMBOL_STR(simple_read_from_buffer) },
	{ 0x28318305, __VMLINUX_SYMBOL_STR(snprintf) },
	{ 0x93fca811, __VMLINUX_SYMBOL_STR(__get_free_pages) },
};

static const char __module_depends[]
__used
__attribute__((section(".modinfo"))) =
"depends=";

